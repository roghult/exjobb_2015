from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize


extensions = [
    Extension("actions/python/*", ["src/actions/python/*.py"],),
    Extension("actions/cython/*", ["src/actions/cython/*.pyx"], language="c++"),
    Extension("data/cython/*", ["src/data/cython/*.pyx"],),
]

setup(
    ext_modules = cythonize(extensions),
)


