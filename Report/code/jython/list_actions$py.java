/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  org.python.compiler.APIVersion
 *  org.python.compiler.Filename
 *  org.python.compiler.MTime
 *  org.python.core.CodeBootstrap
 *  org.python.core.CodeLoader
 *  org.python.core.Py
 *  org.python.core.PyCode
 *  org.python.core.PyFrame
 *  org.python.core.PyFunction
 *  org.python.core.PyFunctionTable
 *  org.python.core.PyList
 *  org.python.core.PyObject
 *  org.python.core.PyRunnable
 *  org.python.core.PyRunnableBootstrap
 *  org.python.core.PyString
 *  org.python.core.ThreadState
 *  org.python.core.imp
 */
package src.actions.python;

import org.python.compiler.APIVersion;
import org.python.compiler.Filename;
import org.python.compiler.MTime;
import org.python.core.CodeBootstrap;
import org.python.core.CodeLoader;
import org.python.core.Py;
import org.python.core.PyCode;
import org.python.core.PyFrame;
import org.python.core.PyFunction;
import org.python.core.PyFunctionTable;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.core.PyRunnable;
import org.python.core.PyRunnableBootstrap;
import org.python.core.PyString;
import org.python.core.ThreadState;
import org.python.core.imp;

@APIVersion(value=36)
@MTime(value=1454597971000L)
@Filename(value="/Users/alexanderroghult/Documents/KTH/exjobb_2015/src/actions/python/list_actions.py")
public class list_actions$py
extends PyFunctionTable
implements PyRunnable {
    static list_actions$py self;
    static final PyCode f$0;
    static final PyCode list_comprehension$1;
    static final PyCode list_append$2;
    static final PyCode list_sort$3;
    static final PyCode f$4;

    public PyObject f$0(PyFrame pyFrame, ThreadState threadState) {
        pyFrame.setline(1);
        PyFunction pyFunction = imp.importOne((String)"time", (PyFrame)pyFrame, (int)-1);
        pyFrame.setlocal("time", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(2);
        pyFunction = new String[]{"Foo"};
        pyFunction = imp.importFrom((String)"data", (String[])pyFunction, (PyFrame)pyFrame, (int)-1);
        String string = pyFunction[0];
        pyFrame.setlocal("Foo", (PyObject)string);
        string = null;
        pyFrame.setline(4);
        pyFunction = imp.importOne((String)"util", (PyFrame)pyFrame, (int)-1);
        pyFrame.setlocal("util", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(8);
        pyFunction = Py.EmptyObjects;
        pyFunction = new PyFunction(pyFrame.f_globals, (PyObject[])pyFunction, list_comprehension$1, null);
        pyFrame.setlocal("list_comprehension", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(26);
        pyFunction = Py.EmptyObjects;
        pyFunction = new PyFunction(pyFrame.f_globals, (PyObject[])pyFunction, list_append$2, null);
        pyFrame.setlocal("list_append", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(51);
        pyFunction = Py.EmptyObjects;
        pyFunction = new PyFunction(pyFrame.f_globals, (PyObject[])pyFunction, list_sort$3, null);
        pyFrame.setlocal("list_sort", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.f_lasti = -1;
        return Py.None;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public PyObject list_comprehension$1(PyFrame var1_1, ThreadState var2_2) {
        var1_1.setline(10);
        if (!var1_1.getlocal(1).__nonzero__()) ** GOTO lbl15
        var1_1.setline(11);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(2, var3_3);
        var3_3 = null;
        var1_1.setline(12);
        v0 = new PyList();
        var3_3 = v0.__getattr__("append");
        var1_1.setlocal(4, var3_3);
        var3_3 = null;
        var1_1.setline(12);
        var3_3 = var1_1.getglobal("range").__call__(var2_2, var1_1.getlocal(0)).__iter__();
        ** GOTO lbl30
lbl15: // 1 sources:
        var1_1.setline(15);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(2, var3_3);
        var3_3 = null;
        var1_1.setline(16);
        v1 = new PyList();
        var3_3 = v1.__getattr__("append");
        var1_1.setlocal(7, var3_3);
        var3_3 = null;
        var1_1.setline(16);
        var3_3 = var1_1.getglobal("range").__call__(var2_2, var1_1.getlocal(0)).__iter__();
        ** GOTO lbl45
lbl-1000: // 1 sources:
        {
            var1_1.setlocal(5, var4_4);
            var1_1.setline(12);
            var1_1.getlocal(4).__call__(var2_2, var1_1.getglobal("Foo").__call__(var2_2, var1_1.getlocal(5)));
lbl30: // 2 sources:
            var1_1.setline(12);
            ** while ((var4_4 = var3_3.__iternext__()) != null)
        }
lbl32: // 1 sources:
        var1_1.setline(12);
        var1_1.dellocal(4);
        var3_3 = v0;
        var1_1.setlocal(3, var3_3);
        var3_3 = null;
        var1_1.setline(13);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(6, var3_3);
        var3_3 = null;
        ** GOTO lbl56
lbl-1000: // 1 sources:
        {
            var1_1.setlocal(5, var4_5);
            var1_1.setline(16);
            var1_1.getlocal(7).__call__(var2_2, var1_1.getlocal(5));
lbl45: // 2 sources:
            var1_1.setline(16);
            ** while ((var4_5 = var3_3.__iternext__()) != null)
        }
lbl47: // 1 sources:
        var1_1.setline(16);
        var1_1.dellocal(7);
        var3_3 = v1;
        var1_1.setlocal(3, var3_3);
        var3_3 = null;
        var1_1.setline(17);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(6, var3_3);
        var3_3 = null;
lbl56: // 2 sources:
        var1_1.setline(19);
        var3_3 = var1_1.getlocal(6)._sub(var1_1.getlocal(2));
        var1_1.setlocal(8, var3_3);
        var3_3 = null;
        var1_1.setline(21);
        var1_1.getglobal("util").__getattr__("print_to_devnull").__call__(var2_2, var1_1.getlocal(3));
        var1_1.setline(22);
        var3_3 = var1_1.getlocal(8);
        var1_1.f_lasti = -1;
        return var3_3;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public PyObject list_append$2(PyFrame var1_1, ThreadState var2_2) {
        var1_1.setline(27);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(1, var3_3);
        var3_3 = null;
        var1_1.setline(28);
        var3_3 = new PyList(Py.EmptyObjects);
        var1_1.setlocal(2, var3_3);
        var3_3 = null;
        var1_1.setline(29);
        var3_3 = Py.newInteger((int)0);
        var1_1.setlocal(3, var3_3);
        var3_3 = null;
        var1_1.setline(30);
        var3_3 = var1_1.getlocal(0).__iter__();
        ** GOTO lbl54
lbl-1000: // 1 sources:
        {
            var1_1.setlocal(4, var4_4);
            var1_1.setline(31);
            var5_5 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
            var1_1.setlocal(5, var5_5);
            var5_5 = null;
            var1_1.setline(33);
            var1_1.getlocal(2).__getattr__("append").__call__(var2_2, var1_1.getlocal(4));
            var1_1.setline(35);
            var5_5 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
            var1_1.setlocal(6, var5_5);
            var5_5 = null;
            var1_1.setline(36);
            var5_5 = var1_1.getlocal(6)._sub(var1_1.getlocal(5));
            var1_1.setlocal(7, var5_5);
            var5_5 = null;
            var1_1.setline(37);
            var5_5 = var1_1.getlocal(3);
            var5_5 = var5_5._iadd(var1_1.getlocal(7));
            var1_1.setlocal(3, var5_5);
            var1_1.setline(38);
            var5_5 = var1_1.getlocal(6)._sub(var1_1.getlocal(1));
            var1_1.setlocal(8, var5_5);
            var5_5 = null;
            var1_1.setline(39);
            var5_5 = var1_1.getlocal(8);
            v0 = var5_5._gt(var1_1.getglobal("util").__getattr__("get_data").__call__(var2_2).__getattr__("TIME_LIMIT"));
            var5_5 = null;
            if (v0.__nonzero__()) {
                var1_1.setline(40);
                Py.println();
                var1_1.setline(41);
                Py.println((PyObject)PyString.fromInterned((String)"Test passed upper time limit, exiting"));
                var1_1.setline(42);
                Py.println();
                var1_1.setline(43);
                var5_5 = var1_1.getglobal("util").__getattr__("get_data").__call__(var2_2).__getattr__("TIME_LIMIT");
                var1_1.f_lasti = -1;
                return var5_5;
            }
lbl54: // 3 sources:
            var1_1.setline(30);
            ** while ((var4_4 = var3_3.__iternext__()) != null)
        }
lbl56: // 1 sources:
        var1_1.setline(46);
        var1_1.getglobal("util").__getattr__("print_to_devnull").__call__(var2_2, var1_1.getlocal(0));
        var1_1.setline(47);
        var5_5 = var1_1.getlocal(3);
        var1_1.f_lasti = -1;
        return var5_5;
    }

    public PyObject list_sort$3(PyFrame pyFrame, ThreadState threadState) {
        PyObject pyObject;
        pyFrame.setline(52);
        if (pyFrame.getlocal(1).__nonzero__()) {
            pyFrame.setline(53);
            pyObject = pyFrame.getglobal("time").__getattr__("clock").__call__(threadState);
            pyFrame.setlocal(2, pyObject);
            pyObject = null;
            pyFrame.setline(54);
            pyObject = new PyObject[3];
            pyObject[0] = pyFrame.getlocal(0);
            pyFrame.setline(54);
            PyObject[] arrpyObject = Py.EmptyObjects;
            pyObject[1] = new PyFunction(pyFrame.f_globals, arrpyObject, f$4);
            pyObject[2] = pyFrame.getglobal("False");
            arrpyObject = new String[]{"key", "reverse"};
            PyObject pyObject2 = pyFrame.getglobal("sorted").__call__(threadState, (PyObject[])pyObject, (String[])arrpyObject);
            pyObject = null;
            pyObject = pyObject2;
            pyFrame.setlocal(3, pyObject);
            pyObject = null;
            pyFrame.setline(55);
            pyObject = pyFrame.getglobal("time").__getattr__("clock").__call__(threadState);
            pyFrame.setlocal(4, pyObject);
            pyObject = null;
        } else {
            pyFrame.setline(57);
            pyObject = pyFrame.getglobal("time").__getattr__("clock").__call__(threadState);
            pyFrame.setlocal(2, pyObject);
            pyObject = null;
            pyFrame.setline(58);
            pyObject = pyFrame.getglobal("sorted").__call__(threadState, pyFrame.getlocal(0));
            pyFrame.setlocal(3, pyObject);
            pyObject = null;
            pyFrame.setline(59);
            pyObject = pyFrame.getglobal("time").__getattr__("clock").__call__(threadState);
            pyFrame.setlocal(4, pyObject);
            pyObject = null;
        }
        pyFrame.setline(61);
        pyObject = pyFrame.getlocal(4)._sub(pyFrame.getlocal(2));
        pyFrame.setlocal(5, pyObject);
        pyObject = null;
        pyFrame.setline(63);
        pyFrame.getglobal("util").__getattr__("print_to_devnull").__call__(threadState, pyFrame.getlocal(3));
        pyFrame.setline(64);
        pyObject = pyFrame.getlocal(5);
        pyFrame.f_lasti = -1;
        return pyObject;
    }

    public PyObject f$4(PyFrame pyFrame, ThreadState threadState) {
        pyFrame.setline(54);
        PyObject pyObject = pyFrame.getlocal(0).__getattr__("index");
        pyFrame.f_lasti = -1;
        return pyObject;
    }

    public list_actions$py(String string) {
        self = this;
        String[] arrstring = new String[]{};
        f$0 = Py.newCode((int)0, (String[])arrstring, (String)string, (String)"<module>", (int)0, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)0, (String[])null, (String[])null, (int)0, (int)4096);
        arrstring = new String[]{"amount", "data_as_objects", "start", "l", "_[12_13]", "x", "stop", "_[16_13]", "elapsed_time"};
        list_comprehension$1 = Py.newCode((int)2, (String[])arrstring, (String)string, (String)"list_comprehension", (int)8, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)1, (String[])null, (String[])null, (int)0, (int)4097);
        arrstring = new String[]{"values", "action_start_time", "l", "total_elapsed_time", "x", "start", "stop", "elapsed_time", "action_elapsed_time"};
        list_append$2 = Py.newCode((int)1, (String[])arrstring, (String)string, (String)"list_append", (int)26, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)2, (String[])null, (String[])null, (int)0, (int)4097);
        arrstring = new String[]{"l", "data_as_objects", "start", "sorted_list", "stop", "elapsed_time"};
        list_sort$3 = Py.newCode((int)2, (String[])arrstring, (String)string, (String)"list_sort", (int)51, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)3, (String[])null, (String[])null, (int)0, (int)4097);
        arrstring = new String[]{"x"};
        f$4 = Py.newCode((int)1, (String[])arrstring, (String)string, (String)"<lambda>", (int)54, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)4, (String[])null, (String[])null, (int)0, (int)4097);
    }

    public PyCode getMain() {
        return f$0;
    }

    public static void main(String[] arrstring) {
        Py.runMain((CodeBootstrap)CodeLoader.createSimpleBootstrap((PyCode)new list_actions$py("src/actions/python/list_actions$py").getMain()), (String[])arrstring);
    }

    public static CodeBootstrap getCodeBootstrap() {
        return PyRunnableBootstrap.getFilenameConstructorReflectionBootstrap((Class)list_actions$py.class);
    }

    public PyObject call_function(int n, PyFrame pyFrame, ThreadState threadState) {
        list_actions$py list_actions$py = this;
        PyFrame pyFrame2 = pyFrame;
        ThreadState threadState2 = threadState;
        switch (n) {
            case 0: {
                return list_actions$py.f$0(pyFrame2, threadState2);
            }
            case 1: {
                return list_actions$py.list_comprehension$1(pyFrame2, threadState2);
            }
            case 2: {
                return list_actions$py.list_append$2(pyFrame2, threadState2);
            }
            case 3: {
                return list_actions$py.list_sort$3(pyFrame2, threadState2);
            }
            case 4: {
                return list_actions$py.f$4(pyFrame2, threadState2);
            }
        }
        return null;
    }
}
