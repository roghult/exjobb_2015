/*
 * Decompiled with CFR 0_110.
 * 
 * Could not load the following classes:
 *  org.python.compiler.APIVersion
 *  org.python.compiler.Filename
 *  org.python.compiler.MTime
 *  org.python.core.CodeBootstrap
 *  org.python.core.CodeLoader
 *  org.python.core.Py
 *  org.python.core.PyCode
 *  org.python.core.PyFrame
 *  org.python.core.PyFunction
 *  org.python.core.PyFunctionTable
 *  org.python.core.PyObject
 *  org.python.core.PyRunnable
 *  org.python.core.PyRunnableBootstrap
 *  org.python.core.PyString
 *  org.python.core.ThreadState
 *  org.python.core.imp
 */
package src.actions.jython;

import org.python.compiler.APIVersion;
import org.python.compiler.Filename;
import org.python.compiler.MTime;
import org.python.core.CodeBootstrap;
import org.python.core.CodeLoader;
import org.python.core.Py;
import org.python.core.PyCode;
import org.python.core.PyFrame;
import org.python.core.PyFunction;
import org.python.core.PyFunctionTable;
import org.python.core.PyObject;
import org.python.core.PyRunnable;
import org.python.core.PyRunnableBootstrap;
import org.python.core.PyString;
import org.python.core.ThreadState;
import org.python.core.imp;

@APIVersion(value=36)
@MTime(value=1455029164000L)
@Filename(value="/Users/alexanderroghult/Documents/KTH/exjobb_2015/src/actions/jython/dictionary_actions.py")
public class dictionary_actions$py
extends PyFunctionTable
implements PyRunnable {
    static dictionary_actions$py self;
    static final PyCode f$0;
    static final PyCode dictionary_insert$1;
    static final PyCode dictionary_merge$2;
    static final PyCode dictionary_read_all_values$3;

    public PyObject f$0(PyFrame pyFrame, ThreadState threadState) {
        pyFrame.setline(1);
        PyFunction pyFunction = imp.importOne((String)"time", (PyFrame)pyFrame, (int)-1);
        pyFrame.setlocal("time", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(3);
        pyFunction = imp.importOne((String)"util", (PyFrame)pyFrame, (int)-1);
        pyFrame.setlocal("util", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(4);
        pyFunction = imp.importOneAs((String)"java.util.HashMap", (PyFrame)pyFrame, (int)-1);
        pyFrame.setlocal("HashMap", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(8);
        pyFunction = Py.EmptyObjects;
        pyFunction = new PyFunction(pyFrame.f_globals, (PyObject[])pyFunction, dictionary_insert$1, null);
        pyFrame.setlocal("dictionary_insert", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(31);
        pyFunction = Py.EmptyObjects;
        pyFunction = new PyFunction(pyFrame.f_globals, (PyObject[])pyFunction, dictionary_merge$2, null);
        pyFrame.setlocal("dictionary_merge", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.setline(46);
        pyFunction = Py.EmptyObjects;
        pyFunction = new PyFunction(pyFrame.f_globals, (PyObject[])pyFunction, dictionary_read_all_values$3, null);
        pyFrame.setlocal("dictionary_read_all_values", (PyObject)pyFunction);
        pyFunction = null;
        pyFrame.f_lasti = -1;
        return Py.None;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public PyObject dictionary_insert$1(PyFrame var1_1, ThreadState var2_2) {
        var1_1.setline(9);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(2, var3_3);
        var3_3 = null;
        var1_1.setline(10);
        var3_3 = var1_1.getglobal("HashMap").__call__(var2_2, var1_1.getlocal(0));
        var1_1.setlocal(3, var3_3);
        var3_3 = null;
        var1_1.setline(11);
        var3_3 = Py.newInteger((int)0);
        var1_1.setlocal(4, var3_3);
        var3_3 = null;
        var1_1.setline(12);
        var3_3 = var1_1.getlocal(1).__iter__();
        ** GOTO lbl60
lbl-1000: // 1 sources:
        {
            var5_5 = Py.unpackSequence((PyObject)var4_4, (int)2);
            var6_6 = var5_5[0];
            var1_1.setlocal(5, var6_6);
            var6_6 = null;
            var6_6 = var5_5[1];
            var1_1.setlocal(6, var6_6);
            var6_6 = null;
            var1_1.setline(13);
            var5_5 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
            var1_1.setlocal(7, var5_5);
            var5_5 = null;
            var1_1.setline(14);
            var1_1.getlocal(3).__getattr__("put").__call__(var2_2, var1_1.getlocal(5), var1_1.getlocal(6));
            var1_1.setline(15);
            var5_5 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
            var1_1.setlocal(8, var5_5);
            var5_5 = null;
            var1_1.setline(16);
            var5_5 = var1_1.getlocal(8)._sub(var1_1.getlocal(7));
            var1_1.setlocal(9, var5_5);
            var5_5 = null;
            var1_1.setline(17);
            var5_5 = var1_1.getlocal(4);
            var5_5 = var5_5._iadd(var1_1.getlocal(9));
            var1_1.setlocal(4, var5_5);
            var1_1.setline(18);
            var5_5 = var1_1.getlocal(8)._sub(var1_1.getlocal(2));
            var1_1.setlocal(10, var5_5);
            var5_5 = null;
            var1_1.setline(19);
            var5_5 = var1_1.getlocal(10);
            v0 = var5_5._gt(var1_1.getglobal("util").__getattr__("get_data").__call__(var2_2).__getattr__("TIME_LIMIT"));
            var5_5 = null;
            if (v0.__nonzero__()) {
                var1_1.setline(20);
                Py.println();
                var1_1.setline(21);
                Py.println((PyObject)PyString.fromInterned((String)"Test passed upper time limit, exiting"));
                var1_1.setline(22);
                Py.println();
                var1_1.setline(23);
                var5_5 = var1_1.getglobal("util").__getattr__("get_data").__call__(var2_2).__getattr__("TIME_LIMIT");
                var1_1.f_lasti = -1;
                return var5_5;
            }
lbl60: // 3 sources:
            var1_1.setline(12);
            ** while ((var4_4 = var3_3.__iternext__()) != null)
        }
lbl62: // 1 sources:
        var1_1.setline(26);
        var1_1.getglobal("util").__getattr__("print_to_devnull").__call__(var2_2, var1_1.getlocal(3));
        var1_1.setline(27);
        var5_5 = var1_1.getlocal(4);
        var1_1.f_lasti = -1;
        return var5_5;
    }

    public PyObject dictionary_merge$2(PyFrame pyFrame, ThreadState threadState) {
        pyFrame.setline(32);
        PyObject pyObject = pyFrame.getglobal("HashMap").__call__(threadState, pyFrame.getlocal(0));
        pyFrame.setlocal(2, pyObject);
        pyObject = null;
        pyFrame.setline(33);
        pyObject = pyFrame.getglobal("HashMap").__call__(threadState, pyFrame.getlocal(1));
        pyFrame.setlocal(3, pyObject);
        pyObject = null;
        pyFrame.setline(35);
        pyObject = pyFrame.getglobal("time").__getattr__("clock").__call__(threadState);
        pyFrame.setlocal(4, pyObject);
        pyObject = null;
        pyFrame.setline(36);
        pyFrame.getlocal(2).__getattr__("putAll").__call__(threadState, pyFrame.getlocal(3));
        pyFrame.setline(37);
        pyObject = pyFrame.getglobal("time").__getattr__("clock").__call__(threadState);
        pyFrame.setlocal(5, pyObject);
        pyObject = null;
        pyFrame.setline(38);
        pyObject = pyFrame.getlocal(5)._sub(pyFrame.getlocal(4));
        pyFrame.setlocal(6, pyObject);
        pyObject = null;
        pyFrame.setline(41);
        pyFrame.getglobal("util").__getattr__("print_to_devnull").__call__(threadState, pyFrame.getlocal(2));
        pyFrame.setline(42);
        pyObject = pyFrame.getlocal(6);
        pyFrame.f_lasti = -1;
        return pyObject;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Lifted jumps to return sites
     */
    public PyObject dictionary_read_all_values$3(PyFrame var1_1, ThreadState var2_2) {
        var1_1.setline(47);
        var3_3 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
        var1_1.setlocal(1, var3_3);
        var3_3 = null;
        var1_1.setline(48);
        var3_3 = var1_1.getglobal("HashMap").__call__(var2_2, var1_1.getlocal(0));
        var1_1.setlocal(2, var3_3);
        var3_3 = null;
        var1_1.setline(49);
        var3_3 = var1_1.getlocal(0).__getattr__("keys").__call__(var2_2);
        var1_1.setlocal(3, var3_3);
        var3_3 = null;
        var1_1.setline(50);
        var3_3 = Py.newInteger((int)0);
        var1_1.setlocal(4, var3_3);
        var3_3 = null;
        var1_1.setline(51);
        var3_3 = var1_1.getlocal(3).__iter__();
        ** GOTO lbl62
lbl-1000: // 1 sources:
        {
            var1_1.setlocal(5, var4_4);
            var1_1.setline(52);
            var5_5 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
            var1_1.setlocal(6, var5_5);
            var5_5 = null;
            var1_1.setline(53);
            var5_5 = var1_1.getlocal(2).__getattr__("get").__call__(var2_2, var1_1.getlocal(5));
            var1_1.setlocal(7, var5_5);
            var5_5 = null;
            var1_1.setline(54);
            var5_5 = var1_1.getglobal("time").__getattr__("clock").__call__(var2_2);
            var1_1.setlocal(8, var5_5);
            var5_5 = null;
            var1_1.setline(55);
            var5_5 = var1_1.getlocal(8)._sub(var1_1.getlocal(6));
            var1_1.setlocal(9, var5_5);
            var5_5 = null;
            var1_1.setline(57);
            var1_1.getglobal("util").__getattr__("print_to_devnull").__call__(var2_2, var1_1.getlocal(7));
            var1_1.setline(58);
            var5_5 = var1_1.getlocal(4);
            var5_5 = var5_5._iadd(var1_1.getlocal(9));
            var1_1.setlocal(4, var5_5);
            var1_1.setline(59);
            var5_5 = var1_1.getlocal(8)._sub(var1_1.getlocal(1));
            var1_1.setlocal(10, var5_5);
            var5_5 = null;
            var1_1.setline(60);
            var5_5 = var1_1.getlocal(10);
            v0 = var5_5._gt(var1_1.getglobal("util").__getattr__("get_data").__call__(var2_2).__getattr__("TIME_LIMIT"));
            var5_5 = null;
            if (v0.__nonzero__()) {
                var1_1.setline(61);
                Py.println();
                var1_1.setline(62);
                Py.println((PyObject)PyString.fromInterned((String)"Test passed upper time limit, exiting"));
                var1_1.setline(63);
                Py.println();
                var1_1.setline(64);
                var5_5 = var1_1.getglobal("util").__getattr__("get_data").__call__(var2_2).__getattr__("TIME_LIMIT");
                var1_1.f_lasti = -1;
                return var5_5;
            }
lbl62: // 3 sources:
            var1_1.setline(51);
            ** while ((var4_4 = var3_3.__iternext__()) != null)
        }
lbl64: // 1 sources:
        var1_1.setline(66);
        var5_5 = var1_1.getlocal(4);
        var1_1.f_lasti = -1;
        return var5_5;
    }

    public dictionary_actions$py(String string) {
        self = this;
        String[] arrstring = new String[]{};
        f$0 = Py.newCode((int)0, (String[])arrstring, (String)string, (String)"<module>", (int)0, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)0, (String[])null, (String[])null, (int)0, (int)4096);
        arrstring = new String[]{"dict_object", "data_values", "action_start_time", "hash_map", "total_elapsed_time", "a", "b", "start", "stop", "elapsed_time", "action_elapsed_time"};
        dictionary_insert$1 = Py.newCode((int)2, (String[])arrstring, (String)string, (String)"dictionary_insert", (int)8, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)1, (String[])null, (String[])null, (int)0, (int)4097);
        arrstring = new String[]{"dict1", "dict2", "hash_map1", "hash_map2", "start", "stop", "elapsed_time"};
        dictionary_merge$2 = Py.newCode((int)2, (String[])arrstring, (String)string, (String)"dictionary_merge", (int)31, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)2, (String[])null, (String[])null, (int)0, (int)4097);
        arrstring = new String[]{"dict_object", "action_start_time", "hash_map", "keys", "total_elapsed_time", "key", "start", "value", "stop", "elapsed_time", "action_elapsed_time"};
        dictionary_read_all_values$3 = Py.newCode((int)1, (String[])arrstring, (String)string, (String)"dictionary_read_all_values", (int)46, (boolean)false, (boolean)false, (PyFunctionTable)self, (int)3, (String[])null, (String[])null, (int)0, (int)4097);
    }

    public PyCode getMain() {
        return f$0;
    }

    public static void main(String[] arrstring) {
        Py.runMain((CodeBootstrap)CodeLoader.createSimpleBootstrap((PyCode)new dictionary_actions$py("src/actions/jython/dictionary_actions$py").getMain()), (String[])arrstring);
    }

    public static CodeBootstrap getCodeBootstrap() {
        return PyRunnableBootstrap.getFilenameConstructorReflectionBootstrap((Class)dictionary_actions$py.class);
    }

    public PyObject call_function(int n, PyFrame pyFrame, ThreadState threadState) {
        dictionary_actions$py dictionary_actions$py = this;
        PyFrame pyFrame2 = pyFrame;
        ThreadState threadState2 = threadState;
        switch (n) {
            case 0: {
                return dictionary_actions$py.f$0(pyFrame2, threadState2);
            }
            case 1: {
                return dictionary_actions$py.dictionary_insert$1(pyFrame2, threadState2);
            }
            case 2: {
                return dictionary_actions$py.dictionary_merge$2(pyFrame2, threadState2);
            }
            case 3: {
                return dictionary_actions$py.dictionary_read_all_values$3(pyFrame2, threadState2);
            }
        }
        return null;
    }
}
