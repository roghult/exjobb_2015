\chapter{Method} \label{section_method}

\section{Project Breakdown}
To reach a result the project was broken down into different tasks, detailed below.

\subsubsection{Code Analysis}
This was the first task performed in the project. 
It consisted of understanding and profiling a code base in order to find generally applicable use cases that could be broken out into tests used in the benchmarks. The code base was provided by TriOptima. Profiling helped to find areas of the code that were slow, which should always be prioritized when optimizing. The bottlenecks of the code that were found through profiling were isolated and modified into generally applicable use cases. The reason for this was to perform the study on programming problems that are general and relevant to the whole programming community, and not specifically for TriOptima. These independent modules were the test cases for all benchmarks and analysis in the study.

\subsubsection{Setup Development Environment}
All work was performed on two computers provided by TriOptima. Before benchmarking or coding could be commenced, the projects development environment needed to be setup. This included installing the interpreters to be used during the project, setting up version control and testing that the code could be run.

\subsubsection{Coding}
The practical part of this project was to implement the independent modules in a manner that each interpreters performance could be measured and recorded. For Cython and Jython a second version of each test was implemented with interpreter specific code. Cython, for example, allows static typing of variables while Jython enables the use of Java data types. The reason for this was to compare the difference between running each interpreter with pure Python code and running with interpreter specific syntax and data types.

\subsubsection{Benchmarking}
Benchmarks were performed for each interpreter, measuring the execution time of each test. The benchmarks were run on two different computers provided by TriOptima. The data gathered from the benchmarks are the result presented in this report, showing the performance of each interpreter for the created test suite.

\section{Hardware}

All experiments and work for this project has been performed on a Mac Pro (Mid 2010) and Dell Latitude E7250 (from here on out referred to as ''Dell''), both provided by TriOptima with the specifications described below.

\subsection{Mac Pro Intel\textregistered\ Xeon\textregistered\ W3530 2.80 GHz 16 GB RAM}
\textbf{Processor:} Intel\textregistered\ Xeon\textregistered\ W3530 2.80 GHz\\
\textbf{Cores:} 4\\
\textbf{L2 Cache:} 256 KB\\
\textbf{L3 Cache:} 8 MB\\
\textbf{Memory:} 16 GB (2x8 GB) 1066 MHz DDR3 ECC 

\subsection{Dell Intel\textregistered\ Core\texttrademark\ i7-5600U CPU @ 2.60GHz 8 GB RAM}
\textbf{Processor:} Intel\textregistered\ Core\texttrademark\ i7-5600U CPU @ 2.60GHz\\
\textbf{Cores:} 2\\
\textbf{L2 Cache:} 256 KB\\
\textbf{L3 Cache:} 4096 KB\\
\textbf{Memory:} 8 GB (1x8 GB) 1600MHz DDR3

\section{Software Environment}
The software used in this project and their respective versions are listed in this section.

\begin{figure}[ht]
	\centering
    \caption{Software used in this project.}
    \label{fig:software}
	\includegraphics[width=\textwidth]{profile/software_trimmed.png}
\end{figure}

% \subsection{Operating system}
% \subsubsection{Dell}
% Ubuntu 15.10.

% \subsubsection{Mac}
% OS X Yosemite 10.10.5.

% \subsection{Python}
% \subsubsection{Dell}
% Python 2.7.10 (default, Oct 14 2015, 16:09:02)
% [GCC 5.2.1 20151010] on linux 2

% \subsubsection{Mac}
% Python 2.7.10 (default, Jul 14 2015, 19:46:27)
% [GCC 4.2.1 Compatible Apple LLVM 6.0 (clang-600.0.39)] on darwin

% \subsection{Cython}
% \subsubsection{Dell}
% Cython version 0.23.4.
% \subsubsection{Mac}
% Cython version 0.23.4.

% \subsection{Jython}
% \subsubsection{Dell}
% Jython 2.7.0 (default:9987c746f838, Apr 29 2015, 02:25:11)
% [Java HotSpot(TM) 64-Bit Server VM (Oracle Corporation)] on java1.8.0\_66

% \subsubsection{Mac}
% Jython 2.7.0 (default:9987c746f838, Apr 29 2015, 02:25:11)
% [Java HotSpot(TM) 64-Bit Server VM (Oracle Corporation)] on java1.8.0\_45

% \subsection{PyPy}
% \subsubsection{Dell}
% Python 2.7.10 (4.0.1+dfsg-1~ppa1~ubuntu15.04, Nov 20 2015, 19:34:27)
% [PyPy 4.0.1 with GCC 4.9.2]
% \subsubsection{Mac}
% Python 2.7.10 (5f8302b8bf9f, Nov 18 2015, 10:38:03)
% [PyPy 4.0.1 with GCC 4.2.1 Compatible Apple LLVM 5.1 (clang-503.0.40)]




\section{Profiling}
Profiling of the TriOptima code base was performed in different steps. During each profiling run, all other running user initiated processes were terminated in order to decrease contention for system resources. Each step was run three times. The purpose of this profiling was to gain knowledge of where most of the execution time was spent. The tests were run with realistic test data that, for disclosure reasons, can not be described in this report.

\subsection{gnu-time}

\begin{figure}[ht]
	\centering
    \caption{Profiling results using gtime.}
    \label{fig:gtime_standard}
	\includegraphics[width=\textwidth]{profile/gtime_standard_trimmed.png}
\end{figure}

Firstly \textit{gnu-time} was used to mainly get information regarding execution time and CPU usage. By using the --verbose flag \textit{gnu-time} outputs user time (seconds), system time (seconds) and percent of CPU the job got. As can be seen in figure \ref{fig:gtime_standard}, out of three runs an average of 44\% of CPU was given to the jobs. This is either an indication that other processes were contending for the CPU or the amount that was spent waiting on I/O.

\subsection{cProfile}

\begin{sidewaysfigure}[p]
	\centering
    \caption{Profiling results using cProfile.}
    \label{fig:cprofile}
	\includegraphics[width=\textwidth]{profile/cProfile_trimmed.png}
\end{sidewaysfigure}

The code provided by TriOptima that was analyzed is part of a large Django \cite{djangosoftwarefoundation_Django} project. Therefore when the program was profiled profilehooks became extremely valuable, allowing profiling of specific functions instead of the whole execution of the program. Despite this, several Django functions showed up in the profiling output since data is fetched from the database through the Django ORM. As these functions were not to be modified in any way, they were disregarded. If any of the runs had a function performing under 10 percent of the total execution time, the method was disregarded out of each run. The results of running cProfile over three runs can be seen in figure \ref{fig:cprofile}.

Profiling using cProfile gave information about each functions execution time. Most parts of TriOptimas code is not public and can therefore not be published in this work. The functions have been renamed in order to be able to show the results from the profiling. Using profilehooks the profiling was started in the programs main entry point, called func\_a. Profiling was started here in order to disregard several Django function calls prior to this function. When examining the output, as seen in figure \ref{fig:cprofile}, it is important to be aware that several of the functions called are Python generators \cite{pythonsoftwarefoundation_Python_Generators}. These functions are marked with ''(g)''. cProfile will mark each retrieval of the next value from a generator as a call to the function. This explains the high values in ncalls for generators.

The functions that drew the authors attention were func\_d, func\_e, func\_n, func\_o, func\_p and func\_q. These have either a high cumtime, a high tottime or both. These functions will provide the basis for the general modules that are to be created later on in the project. Even though functions f-j have high cumtimes these are not relevant to this study as analyzing the code shows that these functions mainly perform retrievals from a database.

\section{Generalizing the Problem}\label{generalization}
In this section the code from the relevant functions from figure  \ref{fig:cprofile} will be described.

\subsection{func\_d}
This is a function that takes an iterable as input. The value in the iterator is a
tuple with a key and value. The function adds the value into a dict at the position
pointed out by the corresponding key. The pseudocode for func\_d can be seen in algorithm \ref{alg:func_d}. \\

\IncMargin{1em}
\RestyleAlgo{boxruled}
\LinesNumbered
\begin{algorithm}[H]
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
	\caption{func\_d - reducing values into a dictionary}\label{alg:func_d}
    \Input{Iterable \textit{i} containing key, value pairs}
    \Output{Dictionary with reduced values in each key}
    \BlankLine
    \ForEach{key, value pair in i}{
        \textit{d}[\textit{key}] $\leftarrow$ \textit{d}[\textit{key}] + \textit{value}
    }
    return \textit{d}
\end{algorithm}
\DecMargin{1em}

\subsection{func\_e}
This function is a generator that takes an iterable as input. A set of factory classes are retrieved. The generator yields a tuple of two objects which are created using each factory for each value of the iterable. The pseudocode for func\_e can be seen in algorithm \ref{alg:func_e}.\\

\IncMargin{1em}
\begin{algorithm}[H]
    \SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
	\caption{func\_e - generator yielding key, value pairs}\label{alg:func_e}
    \Input{Iterable \textit{i} containing instances of data class}
    \Output{tuple of two objects}
    \BlankLine
    factories $\leftarrow$ RetrieveFactories()
    \BlankLine
    \ForEach{data\_instance in i}{
      \ForEach{factory in factories}{
      		\textit{object1} $\leftarrow$ \textit{factory}.createObject1(\textit{data\_instance}) \\
            \textit{object2} $\leftarrow$ \textit{factory}.createObject2(\textit{data\_instance}) \\
            yield (\textit{object1}, \textit{object2})
      }
    }
\end{algorithm}
\DecMargin{1em}

\subsection{func\_n-func\_q}
These functions are all part of the factory classes used in func\_e. The functions create objects and set different values depending on the created object.







\section{Tests}
From the examined functions, described in section \ref{generalization}, the following problems were recognized:
\begin{itemize}
	\item dictionary usage,
    \item list iteration,
    \item tuple iteration,
    \item generators, andt
    \item creating a large collection of objects.
\end{itemize}

From these problems a set of tests were created in order to determine the performance of each interpreter. The tests are described in \cref{sec:dictionary,sec:list,sec:tuple,sec:generator,sec:objects}. Note that for each test there exists two implementations. Each test was first implemented using pure Python code, referred to as \textit{pure Python code} in this report. For Cython and Jython a second implementation of each test was created using data types and syntax which these interpreters allow. These implementations are referred to as \textit{interpreter specific code}. For each test, the test data is generated beforehand and is not part of the time measurement. The test data is identical for each interpreter. For tests that perform an execution several times, as part of a loop, a maximum running time of 15 minutes is allowed to prevent extremely long running times. Generating test data is not part of the measurement. The maximum running time is not upheld for tests that perform one operation, for example the \textit{update} operation performed in the merge test for Python dictionaries, as it would not be possible measuring the operation time during execution without affecting the operation. Time measurement was done using the \textit{time.clock()} function, as this is the recommended function to use for benchmarking Python \cite{pythonsoftwarefoundation_timeclock_documentation}.

Each execution of a test was done by starting a new process of the desired interpreter with the desired amount of data to use in the test. After a test had finished, that test exited its running process and the next test was started by the script. This was done in order to execute each test in isolation from the others. If all tests were run in sequence in the same process, it would be possible for a JIT to utilize compiled code from a previously run test, obscuring the result of that test. All user initiated programs except the terminal running the tests were terminated before benchmarks were performed in this project and all network connections disconnected and disabled. This was done in order to minimize the amount of contention for the CPU.

Below are descriptions of each test. The code for these are available at the project git repository \cite{roghult_Project_git_repository}. Under \path{src/actions} are three folders: \textit{cython}, \textit{jython} and \textit{python}. The \textit{python} folder contains the pure Python code for all tests. The \textit{jython} and \textit{cython} folders contain the interpreter specific code for Jython and Cython respectively. As PyPy has no interpreter specific tests, PyPy simply uses the tests in the \textit{python} folder.

All tests for Jython were run with the option \textit{-J-Xms8g}. This option sets the initial and minimum Java heap size to 8 gigabyte. This was done since the problem sizes in the tests were quite large and so that both the Dell and Mac computers would start with the same heap size.

\subsection{Dictionary}
\label{sec:dictionary}
The tests that were performed on the Python dictionary data type, \textit{dict}, were insertion, overwriting values for existing keys, merging two dictionaries as well as reading values from a given key. All dictionary tests can be found in the \textit{dictionary\_actions.py} files in the project git repository \cite{roghult_Project_git_repository}.

\subsubsection{Insert}
Code for this test can found in the function \textit{dictionary\_insert}.

The insertion test was constructed to test how well each interpreter is able to insert a value for a key previously not occupied. The test is started with an empty dictionary instance and test data, stored in a list of tuples where the tuple contains the key and value to be inserted. All data values are iterated and inserted one by one using the \textit{\_\_setitem\_\_} method ([]), the standard way of setting a value in Python dictionaries. Timestamps are recorded before and after the insert operation and the elapsed time between these values is added to a sum of each iteration. This way only the time of the insertions are recorded, excluding the time of looping through the data. As this test consists of looping through the test data the test will abort if it continues past the maximum running time.

\subsubsection{Overwrite}
Code for this test can be found in the function \textit{dictionary\_insert}.

The overwrite test was constructed to test how well each interpreter is able to change the value at an index already containing a value. The test is started with a dictionary instance and test data, stored in a list of tuples where the tuple contains the key and value to be inserted. All values in the test data are already present in the dictionary instance when the test begins. Apart from this the test is identical to the insertion test.

\subsubsection{Merge}
Code for this test can be found in the function \textit{dictionary\_merge}.

The merge test was constructed to test how well each interpreter is able to merge two dictionary instances using the dictionary method \textit{update}. The test is started with two dictionary instances. The \textit{update} method is then called on the first dictionary with the second dictionary as input, meaning that the second dictionary is merged into the first. This test is run in three different scenarios; using dictionaries that have no matching keys, half of the keys match and all of the keys match. Each method is run separately in order to prevent the tests affecting each others performance. Timestamps are recorded before and after the \textit{update} method and the elapsed time between these values is recorded. As this test consists of only the update method, it is not possible to limit this test to the maximum running time.

\subsubsection{Read}
Code for this test can be found in the function \textit{dictionary\_read\_all\_values}.

The read test was constructed to test how well each interpreter is able to read values from a dictionary. The test is started with a dictionary already containing the test data. The value stored for each existing key in the dictionary is retrieved using the \textit{\_\_getitem\_\_} method ([]). Timestamps are recorded before and after the get operation and the elapsed time between these values is added to a sum of each iteration. As this test consists of looping through the test data, the test will abort if it continues past the maximum running time.


\subsection{List}
\label{sec:list}
The tests that were performed on the Python list data type, \textit{list}, were building lists from list comprehension, appending new values, and sorting. All list tests can be found in the \textit{list\_actions.py} files in the project git repository \cite{roghult_Project_git_repository}.

\subsubsection{List Comprehension}
Code for this test can be found in the function \textit{list\_comprehension}.

The list comprehension test was constructed to test how well each interpreter is able to create a list using list comprehension. The test is started with an integer representing the number of elements that the list should consist of. Timestamps are recorded before and after the list comprehension and the elapsed time between these values is recorded.

\subsubsection{Append}
Code for this test can be found in the function \textit{list\_append}.

The append test was constructed to test how well each interpreter is able to append values to a list. The test is started with a list of values that are iteratively appended to another list which is initially empty. Timestamps are recorded before and after the list comprehension and the elapsed time between these values is added to a sum of each iteration. As this test consists of looping through the test data, the test will abort if it continues past the maximum running time.

\subsubsection{Sort}
Code for this test can be found in the function \textit{list\_sort}.

The sort test was constructed to test how well each interpreter is able to sort a list. The test is started with an unsorted list containing either integers or objects. The test uses the Python function \textit{sorted} to sort the list. Timestamps are recorded before and after the call to \textit{sorted} and the elapsed time between these values is recorded. As this test consists of only calling \textit{sorted}, it is not possible to limit this test to the maximum running time.


\subsection{Tuple}
\label{sec:tuple}
The tests that were performed on the Python \textit{tuple} data type were appending new values and sorting. All tuple tests can be found in the \textit{tuple\_actions.py} files in the project git repository \cite{roghult_Project_git_repository}.

\subsubsection{Append}
Code for this test can be found in the function \textit{tuple\_append}.

The append test was constructed to test how well each interpreter is able to append values to a tuple. The test is started with a list of values that are iteratively appended to a tuple which is initially empty. Since tuples are immutable, the append action actually creates a new tuple with which is a copy of the previous tuple as well as the new value. Timestamps are recorded before and after the append action and the elapsed time between these values is added to a sum of each iteration. As this test consists of looping through the test data, the test will abort if it continues past the maximum running time.

\subsubsection{Sort}
Code for this test can be found in the function \textit{tuple\_sort}.

The sort test was constructed to test how well each interpreter is able to sort a tuple. The test is started with an unsorted tuple containing either integers or objects. The test uses the Python function \textit{sorted} to sort the tuple. Timestamps are recorded before and after the call to \textit{sorted} and the elapsed time between these values is recorded. As this test consists of only calling \textit{sorted}, it is not possible to limit this test to the maximum running time.


\subsection{Generator}
\label{sec:generator}
Code for this test can be found in the function \textit{generators\_iterate} in the \textit{generator\_actions.py} files, located in the project git repository \cite{roghult_Project_git_repository}.

The test that was performed on Python generators was constructed to test how well each interpreter is able to retrieve the next value from a generator that does nothing but yield the next value of the iterator. The test is started with a list of values that are iterated in the generator. As long as there are values to be fetched from the generator the test function gets the next value with the \textit{next} method. Timestamps are recorded before and after the call to \textit{next} and the elapsed time between these values is added to a sum of each iteration. As this test consists of looping through the test data, the test will abort if it continues past the maximum running time.


\subsection{Objects}
\label{sec:objects}
The tests that were performed on objects were creating instances of a class as well as adding two objects together. When two objects are added together in Python the \textit{\_\_add\_\_} method is called. This method was therefore overloaded in the class. The \textit{\_\_str\_\_} and \textit{\_\_repr\_\_} methods are called when a string representation of the object is needed and were also overloaded in the class to make debugging easier. Other than these methods the class only consists of an integer index. All generator tests can be found in the \textit{object\_actions.py} files in the project git repository \cite{roghult_Project_git_repository}.

\subsubsection{Create}
Code for this test can be found in the function \textit{objects\_generate}.

The create test was constructed to test how well each interpreter is able to create objects of a class. The test is started with the amount of objects to be created. Timestamps are recorded before and after each creation of an instance and the elapsed time between these values is added to a sum of each iteration. As this test consists of looping through the test data, the test will abort if it continues past the maximum running time.

\subsubsection{Add}
Code for this test can be found in the function \textit{objects\_add}.

The add test was constructed to test how well each interpreter is able to add two objects together of a class, and store the value into a new variable. The test is started with two lists containing instances of the class. Each object in position \textit{i} in the first list is added with the object in position \textit{i} in the other list and stored in a variable. Timestamps are recorded before and after the add operation and the elapsed time between these values is added to a sum of each iteration. As this test consists of looping through the test data, the test will abort if it continues past the maximum running time.




\section{Test Data}
All tests were run with both integers and instances of a class, created for this project, as values. The class can be found in \path{src/data/__init__.py} as well as \path{src/data/cython/data.pyx} for the Cython interpreter specific in the project git repository \cite{roghult_Project_git_repository}. The problem size used for each test, referred to as \textit{n}, was varied between different runs of each test. Each test was run with \textit{n} being equal to 100, 1\thinspace000, 10\thinspace000, 100\thinspace000, 1\thinspace000\thinspace000 and 5\thinspace000\thinspace000 except for the append test on tuples which did not run the test for \textit{n} equal to 5\thinspace000\thinspace000, as the test would run for an extremely long time. The reason for varying \textit{n} was to register the difference in performance for both small and large sets of data. By varying \textit{n} for the tests it was also believed that the difference in performance for interpreters using a JIT would be visible for the same set.

The keys used for tests performed on the Python dictionary were generated in advance of the tests and stored to text files. The keys are strings of random length between 1 and 1\thinspace000 inclusively, consisting of a random selection of upper case characters, lower case characters and integers. This was done in order to guarantee that the same keys were used across all computers and tests, decreasing the variance in each test run. A result of this was also a speed up in data generation, since each run did not require generation of new keys.






\section{Interpreter Specific Code}
Each test was modified to interpreter specific versions where possible. The Cython and Jython interpreters allow usage of non Python libraries and, in the case of Cython, syntax. PyPy on the other hand does not extend the Python language and does therefore not require any change to the original Python tests. Below are descriptions of the changes made to the Cython and Jython specific implementations.

\subsubsection{Cython}
Calling the \textit{cython} command line program with \textit{-a} on a Cython file generates an HTML file with the Cython code interleaved with the generated C code. In the generated report some lines are highlighted in a shade of yellow. The darker the yellow, the more calls to the Python/C API. Lines that are not highlighted translate to pure C code. This tool was used to add static typing in appropriate areas to decrease the dependence of the Python/C API. For most tests this included adding static typing to variables. When tests were run with objects it was not possible to statically type these. Therefore a check is performed in the beginning of several tests to see if the test is for integers or objects. If the test is for integers the test uses the statically typed variables. The Python class was modified to be an extension type, in order to decrease the dependence of the Python/C API. In the tests performed on lists a C++ vector was used instead of the Python list.

\subsubsection{Jython}
For the Jython specific implementation the Python built in types were replaced by Java counterparts, as these can be imported into the Python source file without the need of writing any Java code. The Python dictionary was replaced with the \textit{java.util.HashMap} and the Python list with \textit{java.util.ArrayList}. By default Python dictionaries translate to a \textit{java.util.concurrent.ConcurrentHashMap}  \cite{jython_PyDictionaryjava_Bitbucket} and Python lists translate to a \textit{java.util.ArrayList} \cite{jython_PyListjava_Bitbucket}.