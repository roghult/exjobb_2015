
### rates Environment Config 
workdir = /Users/alexanderr/Code/trireduce/reduce
DEVRUN = rates
service = rates
ip = 127.0.0.1
database_name = rates
script_name = rates
database_host = predworkdb2
database_port = 33570
database_password = ********
port = 8000
database_user = rates
################################################################################


Let's check some cash flow flatness!

Proposal: #22401 Accepted, Live Execution, Completed, 2015-07-30 (LCH EUR July 2015)
Checking flatness...

Yes! All cash flow amount impacts are within tolerances.

*** PROFILER RESULTS ***
check_and_report (/Users/alexanderr/Code/trireduce/reduce/proposal/management/commands/check_cash_flow_flatness.py:10)
function called 1 times

         370245986 function calls (360929161 primitive calls) in 911.996 seconds

   Ordered by: cumulative time, internal time, call count
   List reduced from 796 to 100 due to restriction <100>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.100    0.100  916.737  916.737 check_cash_flow_flatness.py:10(check_and_report)
        1    0.018    0.018  916.604  916.604 __init__.py:24(is_proposal_cash_flow_amount_impact_flat)
        1    0.001    0.001  915.686  915.686 checker.py:9(calculate_flatness_values_by_unique_flatness_keys)
        1   17.102   17.102  509.040  509.040 checker.py:37(__reduce_values_by_equal_key)
  5255589   12.953    0.000  476.744    0.000 data_structures.py:31(create_flatness_key_value_pairs)
        1    0.000    0.000  338.519  338.519 get_cash_flows.py:10(get_cash_flows_with_metadata)
        1    0.190    0.190  338.183  338.183 get_cash_flows.py:47(__get_relevant_trade_data_by_cmt_key)
   144640    4.208    0.000  337.988    0.002 get_cash_flows.py:54(get_cmt_key_and_trade_data_pairs)
2627796/1313898    4.934    0.000  295.558    0.000 util.py:1777(__call__)
  1313898    2.713    0.000  289.652    0.000 get_cash_flows.py:139(__attach_metadata_to_cash_flows)
  1313898    2.111    0.000  285.658    0.000 get_cash_flows.py:125(__get_relevant_individual_cash_flows)
6446810/4710834    1.002    0.000  182.764    0.000 {len}
  5255588   10.851    0.000  102.438    0.000 data_structures.py:179(create)
  5255588   14.447    0.000   84.087    0.000 data_structures.py:160(calculate_impact_limit_scale_factor)
        1    0.075    0.075   68.116   68.116 checker.py:88(_unwind_factor_by_cycle_master_trade_key)
        1    0.039    0.039   64.681   64.681 util.py:1016(get_proposal_trades)
        1    1.531    1.531   64.611   64.611 util.py:505(get_risk_group_trades)
  5352014   26.162    0.000   56.444    0.000 base.py:468(__eq__)
  1313897   11.731    0.000   39.190    0.000 data_structures.py:234(create)
  1313897   20.176    0.000   30.520    0.000 data_structures.py:63(type_specific_key_function)
20535290/20390651   15.137    0.000   25.160    0.000 lazy.py:35(inner)
        1    4.731    4.731   23.357   23.357 util.py:1094(__make_trade_rows)
31726243/31726183   11.370    0.000   18.297    0.000 {isinstance}
  1325039    8.198    0.000   18.261    0.000 models.py:674(cash_flow_from_pack_format)
  1313897   10.027    0.000   17.369    0.000 data_structures.py:103(ccp_report_key_function)
38373338/33360259   11.984    0.000   16.732    0.000 {getattr}
6827005/6827003    4.646    0.000   16.521    0.000 {setattr}
  5255588    7.668    0.000   12.128    0.000 <string>:1(__ccp_specific_impact_limit_scale_factor)
  5180376    9.150    0.000   11.342    0.000 data_structures.py:200(__add__)
