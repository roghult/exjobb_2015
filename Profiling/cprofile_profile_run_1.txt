
### rates Environment Config 
workdir = /Users/alexanderr/Code/trireduce/reduce
DEVRUN = rates
service = rates
ip = 127.0.0.1
database_name = rates
script_name = rates
database_host = predworkdb2
database_port = 33570
database_password = ********
port = 8000
database_user = rates
################################################################################


Let's check some cash flow flatness!

Proposal: #22401 Accepted, Live Execution, Completed, 2015-07-30 (LCH EUR July 2015)
Checking flatness...

Yes! All cash flow amount impacts are within tolerances.

*** PROFILER RESULTS ***
check_and_report (/Users/alexanderr/Code/trireduce/reduce/proposal/management/commands/check_cash_flow_flatness.py:9)
function called 1 times

         370245986 function calls (360929161 primitive calls) in 1250.923 seconds

   Ordered by: cumulative time, internal time, call count
   List reduced from 796 to 100 due to restriction <100>

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.092    0.092 1255.705 1255.705 check_cash_flow_flatness.py:9(check_and_report)
        1    0.017    0.017 1255.442 1255.442 __init__.py:24(is_proposal_cash_flow_amount_impact_flat)
        1    0.001    0.001 1254.522 1254.522 checker.py:9(calculate_flatness_values_by_unique_flatness_keys)
   621801    3.467    0.000  840.261    0.001 query.py:205(iterator)
        1   17.339   17.339  795.443  795.443 checker.py:37(__reduce_values_by_equal_key)
  5255589   12.847    0.000  762.924    0.000 data_structures.py:31(create_flatness_key_value_pairs)
   144678    2.623    0.000  758.405    0.005 cursor.py:104(execute)
   144678    0.966    0.000  753.833    0.005 utils.py:58(execute)
   144678    0.269    0.000  752.424    0.005 base.py:142(execute)
   144678    1.274    0.000  752.155    0.005 cursors.py:141(execute)
   144678    0.384    0.000  748.106    0.005 cursors.py:317(_query)
   621801    2.869    0.000  742.860    0.001 compiler.py:696(results_iter)
   144678    0.964    0.000  733.467    0.005 cursors.py:279(_do_query)
   144676    1.123    0.000  720.982    0.005 compiler.py:762(execute_sql)
   144678    1.760    0.000  626.667    0.004 cursors.py:119(_do_get_result)
   144678    0.418    0.000  623.553    0.004 cursors.py:315(_get_result)
   144679  623.053    0.004  623.053    0.004 {method 'store_result' of '_mysql.connection' objects}
2627796/1313898    4.749    0.000  584.920    0.000 util.py:1777(__call__)
  1313898    2.589    0.000  579.186    0.000 get_cash_flows.py:139(__attach_metadata_to_cash_flows)
  1313898    2.211    0.000  575.345    0.000 get_cash_flows.py:125(__get_relevant_individual_cash_flows)
        1    0.000    0.000  346.237  346.237 get_cash_flows.py:10(get_cash_flows_with_metadata)
        1    0.184    0.184  345.901  345.901 get_cash_flows.py:47(__get_relevant_trade_data_by_cmt_key)
   144640    4.085    0.000  345.711    0.002 get_cash_flows.py:54(get_cmt_key_and_trade_data_pairs)
  1880320    4.431    0.000  244.573    0.000 related.py:572(__get__)
   144661    1.486    0.000  203.960    0.001 query.py:340(get)
   144675    0.959    0.000  195.623    0.001 query.py:967(_fetch_all)
6446810/4710834    1.042    0.000  180.248    0.000 {len}
   144663    0.216    0.000  179.458    0.001 query.py:121(__len__)
        1    0.070    0.070  112.831  112.831 checker.py:88(_unwind_factor_by_cycle_master_trade_key)
        1    0.035    0.035  109.591  109.591 util.py:1016(get_proposal_trades)
        1    1.500    1.500  109.526  109.526 util.py:505(get_risk_group_trades)
   144679  105.785    0.001  105.785    0.001 {method 'query' of '_mysql.connection' objects}
  5255588   10.882    0.000  100.365    0.000 data_structures.py:179(create)
  5255588   13.920    0.000   82.374    0.000 data_structures.py:160(calculate_impact_limit_scale_factor)
1060686/241065   12.231    0.000   80.125    0.000 query.py:1405(get_cached_row)
  1296746   39.082    0.000   71.750    0.000 base.py:360(__init__)
  5352014   25.644    0.000   55.499    0.000 base.py:468(__eq__)
  1313897   11.630    0.000   38.495    0.000 data_structures.py:234(create)
   289325    0.361    0.000   37.640    0.000 query.py:686(filter)
   289325    1.310    0.000   37.279    0.000 query.py:700(_filter_or_exclude)
   144676    3.457    0.000   33.724    0.000 compiler.py:82(as_sql)
  1313897   19.740    0.000   29.765    0.000 data_structures.py:63(type_specific_key_function)
        1    4.639    4.639   26.688   26.688 util.py:1094(__make_trade_rows)
20535290/20390651   14.812    0.000   24.692    0.000 lazy.py:35(inner)
   289325    1.800    0.000   22.822    0.000 query.py:1312(add_q)
   578651    2.534    0.000   20.454    0.000 query.py:943(_clone)
  1421465    1.836    0.000   20.100    0.000 models.py:689(<genexpr>)
   289325    1.779    0.000   19.099    0.000 query.py:1338(_add_q)
  1325039    8.219    0.000   18.264    0.000 models.py:674(cash_flow_from_pack_format)
31726243/31726183   11.118    0.000   17.637    0.000 {isinstance}
  1313897    9.981    0.000   17.216    0.000 data_structures.py:103(ccp_report_key_function)
6827005/6827003    4.768    0.000   16.874    0.000 {setattr}
38373338/33360259   12.135    0.000   16.611    0.000 {getattr}
   578651    8.561    0.000   16.523    0.000 query.py:240(clone)
       12    0.000    0.000   16.402    1.367 query.py:125(__iter__)
 15322976   12.719    0.000   15.119    0.000 base.py:506(_get_pk_val)
   144676    1.507    0.000   14.764    0.000 compiler.py:192(get_columns)
   144678    0.712    0.000   14.254    0.000 cursors.py:322(_post_get_result)
   144678    0.259    0.000   13.542    0.000 cursors.py:288(_fetch_row)
   144681    2.027    0.000   13.341    0.000 query.py:1152(build_filter)
   144677    7.811    0.000   13.283    0.000 {built-in method fetch_row}
   477125    9.802    0.000   12.725    0.000 compiler.py:6(resolve_columns)
   144682    6.459    0.000   12.603    0.000 compiler.py:274(get_default_columns)
   482132    0.815    0.000   12.105    0.000 subclassing.py:36(__set__)
  5255588    7.482    0.000   11.851    0.000 <string>:1(__ccp_specific_impact_limit_scale_factor)
  5180376    9.235    0.000   11.300    0.000 data_structures.py:200(__add__)
578714/289352    1.302    0.000   10.955    0.000 compiler.py:74(compile)
 18803140   10.224    0.000   10.224    0.000 {method 'get' of 'dictproxy' objects}
   289352    1.012    0.000    9.626    0.000 where.py:85(as_sql)
   433917    1.382    0.000    8.223    0.000 models.py:1222(to_python)
   144681    0.534    0.000    7.833    0.000 lookups.py:148(as_sql)
   144647    0.625    0.000    7.497    0.000 related.py:529(get_queryset)
  5300156    2.781    0.000    7.093    0.000 models.py:671(date_from_pack_format)
  1286142    3.190    0.000    6.579    0.000 data_structures.py:266(__add__)
   433917    1.785    0.000    6.131    0.000 models.py:127(to_python)
144679/144678    0.460    0.000    6.064    0.000 __init__.py:168(cursor)
   144662    0.857    0.000    5.909    0.000 query.py:148(__getitem__)
   144661    0.429    0.000    5.850    0.000 query.py:806(order_by)
   292673    0.791    0.000    5.565    0.000 compiler.py:1152(cursor_iter)
   723316    1.323    0.000    5.445    0.000 copy.py:66(copy)
  1157302    1.999    0.000    4.897    0.000 where.py:300(clone)
  5325554    4.735    0.000    4.735    0.000 {hasattr}
   144681    1.351    0.000    4.729    0.000 lookups.py:138(process_lhs)
   187834    0.396    0.000    4.723    0.000 times.py:93(mysql_timestamp_converter)
  7207536    4.634    0.000    4.634    0.000 {built-in method __new__ of type object at 0x10d3497e8}
  5255588    4.616    0.000    4.616    0.000 data_structures.py:149(calculate_amount_impact)
 10435964    4.557    0.000    4.557    0.000 data_structures.py:185(__init__)
   144647    0.322    0.000    4.467    0.000 manager.py:163(db_manager)
  5255588    4.367    0.000    4.367    0.000 data_structures.py:172(<lambda>)
   187856    3.622    0.000    4.327    0.000 times.py:44(DateTime_or_None)
  4863865    4.312    0.000    4.312    0.000 {built-in method fromordinal}
  8822979    3.960    0.000    3.961    0.000 util.py:1138(__split_field_name)
  9831262    3.836    0.000    3.836    0.000 lazy.py:162(__get__)
  1313897    2.073    0.000    3.686    0.000 data_structures.py:56(per_paydate_key_function)
   144639    3.504    0.000    3.524    0.000 {msgpack._msgpack.unpackb}
   723327    1.666    0.000    3.371    0.000 query.py:51(__init__)
   144665    0.518    0.000    3.357    0.000 models.py:131(__init__)
   144675    0.392    0.000    3.354    0.000 query.py:1131(build_lookup)
   292673    0.978    0.000    3.241    0.000 compiler.py:1158(<lambda>)
  4074479    3.183    0.000    3.183    0.000 {method 'update' of 'dict' objects}


  Command being timed: "./devrun rates manage check_cash_flow_flatness 22401 --create-report-files=no"
  User time (seconds): 529.98
  System time (seconds): 10.01
  Percent of CPU this job got: 42%
  Elapsed (wall clock) time (h:mm:ss or m:ss): 21:21.94
  Average shared text size (kbytes): 0
  Average unshared data size (kbytes): 0
  Average stack size (kbytes): 0
  Average total size (kbytes): 0
  Maximum resident set size (kbytes): 9814720512
  Average resident set size (kbytes): 0
  Major (requiring I/O) page faults: 549
  Minor (reclaiming a frame) page faults: 766941
  Voluntary context switches: 178716
  Involuntary context switches: 13547
  Swaps: 0
  File system inputs: 18
  File system outputs: 449
  Socket messages sent: 144695
  Socket messages received: 204385
  Signals delivered: 0
  Page size (bytes): 4096
  Exit status: 0