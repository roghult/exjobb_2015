#!/usr/bin/env bash
ORIG_PATH=$PATH
export PATH="/Users/alexanderr/.trioptima-virtual-environments/trireduce/bin:$PATH"
cd ~/Code/trireduce/;

echo "Running profile 1:"
gtime --verbose ./devrun rates manage check_cash_flow_flatness 22401 --create-report-files=no > /Users/alexanderr/Documents/exjobb_2015/Profiling/gtime_profile_run_1.txt;
echo;
echo;

echo "Running profile 2:"
gtime --verbose ./devrun rates manage check_cash_flow_flatness 22401 --create-report-files=no > /Users/alexanderr/Documents/exjobb_2015/Profiling/gtime_profile_run_2.txt;
echo;
echo;

echo "Running profile 3:"
gtime --verbose ./devrun rates manage check_cash_flow_flatness 22401 --create-report-files=no > /Users/alexanderr/Documents/exjobb_2015/Profiling/gtime_profile_run_3.txt;


cd -
export PATH="$ORIG_PATH"
echo "Done!"