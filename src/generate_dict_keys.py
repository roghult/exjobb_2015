import data

def main():
    amounts = [100, 1000, 10000, 100000, 1000000, 5000000]
    for amount in amounts:
        print
        print "amount = {}".format(amount)
        match = 0
        print "dict match = {}".format(match)
        data.get_dict(amount, False, match)

        match = amount / 2
        print "dict match = {}".format(match)
        data.get_dict(amount, False, match)

        match = amount
        print "dict match = {}".format(match)
        data.get_dict(amount, False, match)

        print "unsorted list"
        data.get_unsorted_list(amount, False)


if __name__ == '__main__':
    main()
