CHARTS = {
    # Append
    # 'Tuple, Append, Integers 0-10000': {
    #     'function': 'tuple',
    #     'action': 'append',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Tuple, Append, Objects 0-10000': {
    #     'function': 'tuple',
    #     'action': 'append',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'Tuple, Append, Integers 100000-1000000': {
        'function': 'tuple',
        'action': 'append',
        'amount_min': 100000,
        'amount_max': 1000000,
        'data_type': 'integer',
    },
    'Tuple, Append, Objects 100000-1000000': {
        'function': 'tuple',
        'action': 'append',
        'amount_min': 100000,
        'amount_max': 1000000,
        'data_type': 'object',
    },
    # Sort
    # 'Tuple, Sort, Integers 0-10000': {
    #     'function': 'tuple',
    #     'action': 'sort',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Tuple, Sort, Objects 0-10000': {
    #     'function': 'tuple',
    #     'action': 'sort',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'Tuple, Sort, Integers 100000-5000000': {
        'function': 'tuple',
        'action': 'sort',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Tuple, Sort, Objects 100000-5000000': {
        'function': 'tuple',
        'action': 'sort',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
}
