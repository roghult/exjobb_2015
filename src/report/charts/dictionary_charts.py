CHARTS = {
    # INSERT
    # 'Dictionary, Insert Integers, 0-10000': {
    #     'function': 'dict',
    #     'action': 'insert',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Dictionary, Insert Objects, 0-10000': {
    #     'function': 'dict',
    #     'action': 'insert',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object'
    # },
    'Dictionary, Insert Integers, 100000-5000000': {
        'function': 'dict',
        'action': 'insert',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Dictionary, Insert Objects, 100000-5000000': {
        'function': 'dict',
        'action': 'insert',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object'
    },
    # OVERWRITE
    # 'Dictionary, Overwrite Integers, 0-10000': {
    #     'function': 'dict',
    #     'action': 'overwrite',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Dictionary, Overwrite Objects, 0-10000': {
    #     'function': 'dict',
    #     'action': 'overwrite',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object'
    # },
    'Dictionary, Overwrite Integers, 100000-5000000': {
        'function': 'dict',
        'action': 'overwrite',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Dictionary, Overwrite Objects, 100000-5000000': {
        'function': 'dict',
        'action': 'overwrite',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object'
    },
    # Merge all match
    # 'Dictionary, Merge All Match Integers, 0-10000': {
    #     'function': 'dict',
    #     'action': 'merge_all_match',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Dictionary, Merge All Match Objects, 0-10000': {
    #     'function': 'dict',
    #     'action': 'merge_all_match',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object'
    # },
    'Dictionary, Merge All Match Integers, 100000-5000000': {
        'function': 'dict',
        'action': 'merge_all_match',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Dictionary, Merge All Match Objects, 100000-5000000': {
        'function': 'dict',
        'action': 'merge_all_match',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object'
    },
    # Merge half match
    # 'Dictionary, Merge Half Match Integers, 0-10000': {
    #     'function': 'dict',
    #     'action': 'merge_half_match',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Dictionary, Merge Half Match Objects, 0-10000': {
    #     'function': 'dict',
    #     'action': 'merge_half_match',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object'
    # },
    'Dictionary, Merge Half Match Integers, 100000-5000000': {
        'function': 'dict',
        'action': 'merge_half_match',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Dictionary, Merge Half Match Objects, 100000-5000000': {
        'function': 'dict',
        'action': 'merge_half_match',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object'
    },
    # Merge none match
    # 'Dictionary, Merge None Match Integers, 0-10000': {
    #     'function': 'dict',
    #     'action': 'merge_none_match',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Dictionary, Merge None Match Objects, 0-10000': {
    #     'function': 'dict',
    #     'action': 'merge_none_match',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object'
    # },
    'Dictionary, Merge None Match Integers, 100000-5000000': {
        'function': 'dict',
        'action': 'merge_none_match',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Dictionary, Merge None Match Objects, 100000-5000000': {
        'function': 'dict',
        'action': 'merge_none_match',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object'
    },
    # Merge read
    # 'Dictionary, Read Integers, 0-10000': {
    #     'function': 'dict',
    #     'action': 'read',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'Dictionary, Read Objects, 0-10000': {
    #     'function': 'dict',
    #     'action': 'read',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object'
    # },
    'Dictionary, Read Integers, 100000-5000000': {
        'function': 'dict',
        'action': 'read',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Dictionary, Read Objects, 100000-5000000': {
        'function': 'dict',
        'action': 'read',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object'
    },
}
