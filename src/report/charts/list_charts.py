CHARTS = {
    # List comprehension
    # 'List, List Comprehension, Integers 0-10000': {
    #     'function': 'list',
    #     'action': 'list_comprehension',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'List, List Comprehension, Objects 0-10000': {
    #     'function': 'list',
    #     'action': 'list_comprehension',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'List, List Comprehension, Integers 100000-5000000': {
        'function': 'list',
        'action': 'list_comprehension',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'List, List Comprehension, Objects 100000-5000000': {
        'function': 'list',
        'action': 'list_comprehension',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
    # Append
    # 'List, Append, Integers 0-10000': {
    #     'function': 'list',
    #     'action': 'append',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'List, Append, Objects 0-10000': {
    #     'function': 'list',
    #     'action': 'append',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'List, Append, Integers 100000-5000000': {
        'function': 'list',
        'action': 'append',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'List, Append, Objects 100000-5000000': {
        'function': 'list',
        'action': 'append',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
    # Sort
    # 'List, Sort, Integers 0-10000': {
    #     'function': 'list',
    #     'action': 'sort',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'integer',
    # },
    # 'List, Sort, Objects 0-10000': {
    #     'function': 'list',
    #     'action': 'sort',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'List, Sort, Integers 100000-5000000': {
        'function': 'list',
        'action': 'sort',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'List, Sort, Objects 100000-5000000': {
        'function': 'list',
        'action': 'sort',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
}