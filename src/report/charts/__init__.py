import dictionary_charts
import generator_charts
import list_charts
import object_charts
import tuple_charts


def merge(d1, d2):
    copy = d1.copy()
    copy.update(d2)
    return copy

CHARTS = merge(dictionary_charts.CHARTS,
               merge(generator_charts.CHARTS,
                     merge(list_charts.CHARTS,
                           merge(object_charts.CHARTS,
                                 tuple_charts.CHARTS))))
