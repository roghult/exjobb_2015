CHARTS = {
    # Generate
    # 'Objects, Generate 0-10000': {
    #     'function': 'objects',
    #     'action': 'generate',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'Objects, Generate 100000-5000000': {
        'function': 'objects',
        'action': 'generate',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
    # Add
    # 'Objects, Add 0-10000': {
    #     'function': 'objects',
    #     'action': 'add',
    #     'amount_min': 0,
    #     'amount_max': 10000,
    #     'data_type': 'object',
    # },
    'Objects, Add 100000-5000000': {
        'function': 'objects',
        'action': 'add',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
}