CHARTS = {
    # 'Generator, Generate Integers, 0-100000': {
    #     'function': 'generator',
    #     'action': 'generate',
    #     'amount_min': 0,
    #     'amount_max': 100000,
    #     'data_type': 'integer',
    # },
    # 'Generator, Generate Objects, 0-100000': {
    #     'function': 'generator',
    #     'action': 'generate',
    #     'amount_min': 0,
    #     'amount_max': 100000,
    #     'data_type': 'object',
    # },
    'Generator, Generate Integers, 100000-5000000': {
        'function': 'generator',
        'action': 'generate',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'integer',
    },
    'Generator, Generate Objects, 100000-5000000': {
        'function': 'generator',
        'action': 'generate',
        'amount_min': 100000,
        'amount_max': 5000000,
        'data_type': 'object',
    },
}