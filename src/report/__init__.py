import os
import pygal
import json
import charts
from pygal.style import Style
import csv


CHARTS = charts.CHARTS

INTERPRETERS = ['CPython', 'PyPy', 'Jython', 'Cython']
VARIATIONS_FOLDER_PATH = "../variations/"


EXPECTED_X_VALUES = [100, 1000, 10000, 100000, 1000000, 5000000]


def get_mac_base_code_data_file():
    return open('../data/saved_time/final/mac/time-base_code-merged.txt', mode="r")


def get_mac_interpreter_code_data_file():
    return open('../data/saved_time/final/mac/time-interpreter_code-merged.txt', mode="r")


def get_dell_base_code_data_file():
    return open('../data/saved_time/final/dell/time-base_code-merged.txt', mode="r")


def get_dell_interpreter_code_data_file():
    return open('../data/saved_time/final/dell/time-interpreter_code-merged.txt', mode="r")


def get_data(data_file):
    data = {}
    with data_file:
        for line in data_file:
            parsed_json = json.loads(line)

            amount = parsed_json['amount']
            interpreter = parsed_json['interpreter']
            data_type = parsed_json['data_type']
            action = parsed_json['action']
            function = parsed_json['function']

            function_entry = data.get(function, {})
            action_entry = function_entry.get(action, {})
            data_type_entry = action_entry.get(data_type, {})
            interpreter_entry = data_type_entry.get(interpreter, {})
            time_values = interpreter_entry.get(amount, [])
            time_values.append(parsed_json['time'])

            interpreter_entry[amount] = time_values
            data_type_entry[interpreter] = interpreter_entry
            action_entry[data_type] = data_type_entry
            function_entry[action] = action_entry
            data[function] = function_entry

    return data


def create_report(data, computer, base_code=False):
    custom_style = Style(
        background='white',
        plot_background='white',
        foreground='#000000',
        foreground_strong='#000000',
        foreground_subtle='#000000',
        legend_font_size=30)

    for name, properties in CHARTS.iteritems():
        chart_x_values = set()
        if base_code:
            name += " Python Code"
        else:
            name += " Interpreter Code"
        name += ", " + computer
        function = properties['function']
        action = properties['action']
        data_type = properties['data_type']
        amount_min = int(properties['amount_min'])
        amount_max = int(properties['amount_max'])

        chart = pygal.Bar(style=custom_style)
        chart.title = name

        for interpreter in INTERPRETERS:
            interpreter_values = []
            amount_values = data[function][action][data_type][interpreter]
            for amount, values in amount_values.iteritems():
                if amount_min <= int(amount) <= amount_max:
                    smallest_value = min(values)
                    if function == "dict" and action == "read" and data_type == "object" and interpreter == "Jython" and smallest_value == 900:
                        smallest_value = 0
                    interpreter_values.append((amount, smallest_value))
                    chart_x_values.add(amount)

            # make sure that all x values are present, otherwise add a None data point
            interpreter_x_values = [x[0] for x in interpreter_values]
            for x_value in EXPECTED_X_VALUES:
                if x_value not in interpreter_x_values and (amount_min <= x_value <= amount_max):
                    interpreter_values.append((x_value, None))
                    interpreter_x_values.append(x_value)
                    chart_x_values.add(x_value)

            interpreter_values = sorted(interpreter_values, key=lambda x: x[0], reverse=False)
            y_values = [x[1] for x in interpreter_values]
            chart.add(interpreter, y_values)

        chart.x_labels = sorted(list(chart_x_values))
        chart.render_to_file("../graphs/{}.svg".format(name.replace(' ', '').replace(',', '').replace('(', '').replace(')', '')))


def write_variation_tex(computer, base_code, sorted_variations):
    if not os.path.exists(VARIATIONS_FOLDER_PATH):
        os.makedirs(VARIATIONS_FOLDER_PATH)
    if base_code:
            code = " Python Code"
    else:
        code = " Interpreter Code"
    variation_file = VARIATIONS_FOLDER_PATH + computer + "_" + code + "_" + "variation.tex"
    variation_file = variation_file.replace(' ', '')
    print variation_file

    with open(variation_file, 'w') as f:
        f.write("\\small\n")
        f.write("\\setlength\\tabcolsep{2pt}\n")
        f.write("\\setlength\LTleft{-1in}\n")
        f.write("\\setlength\LTright{-1in plus 1 fill}\n")
        f.write("\\begin{center}\n")
        f.write("\\begin{longtable}{| l | l | l | l | l | l | l | l |}\n")
        f.write("\hline\n")
        f.write("\\textbf{Interpreter} & \\textbf{Function} & \\textbf{Action} & \\textbf{Amount} & \\textbf{Type} & \\textbf{Smallest Value} & \\textbf{Largest Value} & \\textbf{Largest Variation (s)} \\\\ \hline\n")
        f.write("\endhead\n")
        for variation in sorted_variations:
            line = ""
            for element in variation:
                line += str(element).replace("_", " ") + " & "
            f.write(line[:-3] + " \\\\ \hline" + '\n')
        f.write("\end{longtable}\n")
        f.write("\end{center}\n")


def write_variation_csv(computer, base_code, sorted_variations):
    if not os.path.exists(VARIATIONS_FOLDER_PATH):
        os.makedirs(VARIATIONS_FOLDER_PATH)
    if base_code:
            code = " Python Code"
    else:
        code = " Interpreter Code"
    variation_file = VARIATIONS_FOLDER_PATH + computer + "_" + code + "_" + "variation.csv"
    variation_file = variation_file.replace(' ', '')
    print variation_file

    with open(variation_file, 'w') as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow(["Interpreter", "Function", "Action", "Amount", "Type", "Smallest Value", "Largest Value", "Largest Variation (s)", "Largest Variation (%)"])
        for variation in sorted_variations:
            csv_writer.writerow(variation)


def calculate_variations(data, computer, base_code=False):
    functions_and_actions = {
        "dict": ["insert", "overwrite", "read", "merge_all_match", "merge_half_match", "merge_none_match"],
        "list": ["list_comprehension", "append", "sort"],
        "generator": ["generate"],
        "objects": ["generate", "add"],
        "tuple": ["append", "sort"]
    }
    data_types = ["integer", "object"]
    variations = []

    for interpreter in INTERPRETERS:
        number_of_values = 0
        for function, actions in functions_and_actions.iteritems():
            for action in actions:
                for data_type in data_types:
                    try:
                        amount_values = data[function][action][data_type][interpreter]
                        for amount, values in amount_values.iteritems():
                            smallest_value = min(values)
                            largest_value = max(values)
                            largest_variation_sec = largest_value - smallest_value
                            if largest_variation_sec == 0:
                                print interpreter + " - " + function + " - " + action + " - " + str(amount)
                                largest_variation_percent = 0
                            else:
                                largest_variation_percent = (largest_variation_sec / smallest_value) * 100
                            number_of_values += 1
                            variations.append((interpreter, function, action, amount, data_type, smallest_value, largest_value, largest_variation_sec, largest_variation_percent))
                    except KeyError as e:
                        # do nothing
                        pass

    sorted_variations = sorted(variations, key=lambda element: (element[1], element[2], element[3], element[4]))

    write_variation_csv(computer, base_code, sorted_variations)
    # write_variation_tex(computer, base_code, sorted_variations)

