from functools import wraps
import importlib
import os
import time
import sys
import json


def debug(text):
    if get_data().DEBUG:
        print text


def get_time_logfile():
    if get_data().BASE:
        code = "-base_code"
    else:
        code = "-interpreter_code"
    return open('../data/time/time{}.txt'.format(code), mode="a")


def get_memory_logfile():
    data = get_data()
    return open('../data/memory/{}-{}.txt'.format(data.INTERPRETER, data.DATE), mode="a")


PRETTY_INTERPRETER = {
    'python': 'CPython',
    'cython': 'Cython',
    'jython': 'Jython',
    'pypy': 'PyPy',
}


def log(stream, elapsed_time, data_as_objects, name=None):
    data = get_data()
    if name is None:
        name = data.ACTION
    if data_as_objects:
        data_type = 'object'
    else:
        data_type = 'integer'
    new_entry = {
        'interpreter': PRETTY_INTERPRETER[data.INTERPRETER],
        'function': data.FUNCTION,
        'action': name,
        'amount': data.AMOUNT,
        'time': elapsed_time,
        'data_type': data_type
    }
    with stream:
        json.dump(new_entry, stream)
        stream.write('\n')


def profile_time(func=None, stream=None):
    if func is not None:
        @wraps(func)
        def wrapper(*args, **kwargs):
            start = time.clock()
            result = func(*args, **kwargs)
            stop = time.clock()
            log(stream, stop-start)
            print "@timethis: " + func.func_name + " took " + str(stop - start) + " seconds."
            return result
        return wrapper
    else:
        def inner_wrapper(f):
            return profile_time(f, stream=stream)
        return inner_wrapper


def profile_memory(func=None):
    data = get_data()
    if func is not None:
        logfile = get_memory_logfile()
        from memory_profiler import profile
        mem_profiler = profile(func=func, stream=logfile)

        @wraps(mem_profiler)
        def wrapper(*args, **kwargs):
            logfile.write("{}.{}.{}\n".format(data.FUNCTION, data.ACTION, data.AMOUNT))
            result = mem_profiler(*args, **kwargs)
            return result
        return wrapper
    else:
        def inner_wrapper(f):
            return profile_memory(f)
        return inner_wrapper


def empty_wrapper(func=None):
    if func is not None:
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            return result
        return wrapper
    else:
        def inner_wrapper(f):
            return empty_wrapper(f)
        return inner_wrapper


def profiler(func=None):
    data = get_data()
    if data.PROFILE_METHOD == 'time':
        logfile = get_time_logfile()
        return profile_time(func=func, stream=logfile)
    elif data.PROFILE_METHOD == 'memory':
        return profile_memory(func)
    else:
        return empty_wrapper(func=func)


def get_actions(action):
    data = get_data()
    if data.BASE or data.INTERPRETER == "pypy":
        # if code should be run on base python code, get python modules
        module = "actions.{}.{}".format("python", action)
        desired_action = importlib.import_module(module)
        return desired_action
    else:
        module = "actions.{}.{}".format(data.INTERPRETER, action)
        desired_action = importlib.import_module(module)
        return desired_action


def get_data():
    import data as python_data
    if python_data.BASE:
        return python_data
    elif python_data.INTERPRETER == "cython":
        import data.cython.data as cython_data
        return cython_data
    else:
        return python_data


def print_to_devnull(x):
    old_stdout = sys.stdout
    with open(os.devnull, 'w') as f:
        sys.stdout = f
        try:
            print len(x)
        except TypeError:
            print x
    sys.stdout = old_stdout


def copy_data():
    import data as python_data

    if python_data.INTERPRETER == "cython":
        import data.cython.data as cython_data
        cython_data.AMOUNT = python_data.AMOUNT
        cython_data.DEBUG = python_data.DEBUG
        cython_data.INTERPRETER = python_data.INTERPRETER
        cython_data.PROFILE_METHOD = python_data.PROFILE_METHOD
        cython_data.BASE = python_data.BASE
        cython_data.DATA_AS_OBJECTS = python_data.DATA_AS_OBJECTS
        cython_data.MERGE_METHOD = python_data.MERGE_METHOD
