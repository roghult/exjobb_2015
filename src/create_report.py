import os
import subprocess
import report
from os import listdir
from os.path import isfile, join


PROJECT_FOLDER_PATH = os.getcwd() + '/../'
GRAPH_FOLDER_PATH = PROJECT_FOLDER_PATH + 'graphs/'
PDF_FOLDER_PATH = GRAPH_FOLDER_PATH + 'pdf/'


DELL_FILES_TO_PDF = [
    'DictionaryOverwriteIntegers100000-5000000InterpreterCodeDell.svg',
    'DictionaryOverwriteIntegers100000-5000000PythonCodeDell.svg',
    'DictionaryOverwriteObjects100000-5000000InterpreterCodeDell.svg',
    'DictionaryOverwriteObjects100000-5000000PythonCodeDell.svg',
    'ListAppendIntegers100000-5000000InterpreterCodeDell.svg',
    'ListAppendObjects100000-5000000InterpreterCodeDell.svg',
    'ObjectsAdd100000-5000000InterpreterCodeDell.svg',
    'ObjectsAdd100000-5000000PythonCodeDell.svg',
    'DictionaryMergeHalfMatchObjects100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeHalfMatchObjects100000-5000000PythonCodeDell.svg',
    'DictionaryInsertIntegers100000-5000000InterpreterCodeDell.svg',
    'DictionaryInsertIntegers100000-5000000PythonCodeDell.svg',
    'DictionaryInsertObjects100000-5000000PythonCodeDell.svg',
    'DictionaryMergeAllMatchIntegers100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeAllMatchIntegers100000-5000000PythonCodeDell.svg',
    'DictionaryMergeAllMatchObjects100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeAllMatchObjects100000-5000000PythonCodeDell.svg',
    'DictionaryMergeHalfMatchIntegers100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeHalfMatchIntegers100000-5000000PythonCodeDell.svg',
    'DictionaryMergeHalfMatchObjects100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeHalfMatchObjects100000-5000000PythonCodeDell.svg',
    'DictionaryMergeNoneMatchIntegers100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeNoneMatchIntegers100000-5000000PythonCodeDell.svg',
    'DictionaryMergeNoneMatchObjects100000-5000000InterpreterCodeDell.svg',
    'DictionaryMergeNoneMatchObjects100000-5000000PythonCodeDell.svg',
    'DictionaryOverwriteObjects100000-5000000InterpreterCodeDell.svg'
]


def should_convert_svg_to_pdf(svg_file):
    if 'MacPro.svg' in svg_file:
        return True

    if svg_file in DELL_FILES_TO_PDF:
        return True

    return False


def create_pdfs():
    # use Inkscape to convert charts to PDF
    svg_file_names = [f for f in listdir(GRAPH_FOLDER_PATH) if isfile(join(GRAPH_FOLDER_PATH, f)) and f.endswith('.svg')]

    if not os.path.exists(PDF_FOLDER_PATH):
        os.makedirs(PDF_FOLDER_PATH)

    for file_name in svg_file_names:
        svg_file = GRAPH_FOLDER_PATH + file_name
        pdf_file_name = file_name.replace('.svg', '.pdf')
        export_file = PDF_FOLDER_PATH + pdf_file_name
        if should_convert_svg_to_pdf(file_name):
            subprocess.call(['inkscape', '-z', '-D', '--file={}'.format(svg_file), '--export-pdf={}'.format(export_file), '--export-latex'])


def get_latex_figure_text(figure_name):
    label = figure_name.replace('.pdf', '')
    caption = figure_name
    text = """
\\begin{{figure}}
    \centering
    \caption{{{}}}
    \label{{{}}}
    \def\svgwidth{{0.9\columnwidth}}
    \input{{graphs/{}_tex}}
\end{{figure}}
    """.format("", label, figure_name)
    return text



def write_latex_file():
    graphs_tex_file = PDF_FOLDER_PATH + 'all_graphs.tex'
    with open(graphs_tex_file, mode='w') as tex_file:
        pdf_file_names = [f for f in listdir(PDF_FOLDER_PATH) if isfile(join(PDF_FOLDER_PATH, f)) and f.endswith('.pdf')]
        for index, file_name in enumerate(pdf_file_names):
            text = get_latex_figure_text(file_name)
            if index % 15 == 0:
                tex_file.write('\clearpage')
            tex_file.write(text)


def correct_pdf_tex_files():
    pdf_tex_file_names = [f for f in listdir(PDF_FOLDER_PATH) if isfile(join(PDF_FOLDER_PATH, f)) and f.endswith('.pdf_tex')]
    for file_name in pdf_tex_file_names:
        file_path = PDF_FOLDER_PATH + file_name
        with open(file_path, 'r') as f:
            data = f.readlines()

        replace_string = '{\includegraphics[width=\unitlength]{'
        new_string = '{\includegraphics[width=\unitlength]{graphs/'

        with open(file_path, 'w') as f:
            for line in data:
                line = line.replace(replace_string, new_string)
                f.write(line)


def main():
    mac_data = report.get_data(report.get_mac_base_code_data_file())
    report.create_report(mac_data, "Mac Pro", base_code=True)
    report.calculate_variations(mac_data, "Mac Pro", base_code=True)

    mac_data = report.get_data(report.get_mac_interpreter_code_data_file())
    report.create_report(mac_data, "Mac Pro", base_code=False)
    report.calculate_variations(mac_data, "Mac Pro", base_code=False)

    dell_data = report.get_data(report.get_dell_base_code_data_file())
    report.create_report(dell_data, "Dell", base_code=True)
    report.calculate_variations(dell_data, "Dell", base_code=True)

    dell_data = report.get_data(report.get_dell_interpreter_code_data_file())
    report.create_report(dell_data, "Dell ", base_code=False)
    report.calculate_variations(dell_data, "Dell ", base_code=False)

    create_pdfs()

    write_latex_file()

    correct_pdf_tex_files()

if __name__ == '__main__':
    main()
