import os
import random
from datetime import datetime
import string

DEBUG = False
PROFILE_METHOD = None
DATE = datetime.today().strftime("%Y-%m-%d")
FUNCTION = None
ACTION = None
AMOUNT = None
INTERPRETER = None
BASE = False
DATA_AS_OBJECTS = False
MERGE_METHOD = ""
TIME_LIMIT = 60 * 15


class Foo(object):

    def __init__(self, index):
        self.index = index

    def __str__(self):
        return 'Foo({})'.format(self.index)

    def __repr__(self):
        return 'Foo({})'.format(self.index)

    def __add__(self, other):
        return self.index + other.index


def _fill_keys(keys, desired_size, already_used_keys=[]):
    already_used_key_set = set(already_used_keys)
    key_set = set(keys)
    while len(key_set) < desired_size:
        # randomize key length
        key_length = random.randint(1, 1000)
        # randomize key containing letters and digits
        randomized_key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(key_length))
        if randomized_key not in already_used_key_set:
            key_set.add(randomized_key)

    return list(key_set)


DICT_KEYS_FILE_NAME = "dict_keys/dict_keys_{}.txt"
OVERLAPPING_DICT_KEYS_FILE_NAME = "dict_keys/overlapping_dict_keys_{}-{}.txt"


def _get_dict_keys(amount):
    file_name = DICT_KEYS_FILE_NAME.format(amount)
    if not os.path.isfile(file_name):
        with open(file_name, mode='w') as f:
            randomized_keys = _fill_keys([], amount)
            for key in randomized_keys:
                f.write(key)
                f.write('\n')

            return randomized_keys
    else:
        with open(file_name, mode='r') as f:
            keys = []
            for key in f:
                key = key.replace('\n', '')
                keys.append(key)
            if keys[-1] == '':
                del keys[-1]
            assert len(keys) == amount
            return keys


def get_dict_data(amount, data_as_objects=False):
    dict_keys = _get_dict_keys(amount)
    data = []
    for i, x in enumerate(dict_keys):
        if data_as_objects:
            data.append((x, Foo(i)))
        else:
            data.append((x, i))
    return data


def _get_overlapping_dict(dict_size, start, data_as_objects):
    file_name = OVERLAPPING_DICT_KEYS_FILE_NAME.format(dict_size, start)
    if not os.path.isfile(file_name):
        with open(file_name, mode='w') as f:
            existing_keys = _get_dict_keys(dict_size)
            overlapping_keys = existing_keys[start:]
            keys = _fill_keys(overlapping_keys, dict_size, existing_keys)
            for key in keys:
                f.write(key)
                f.write('\n')

    else:
        with open(file_name, mode='r') as f:
            keys = []
            for key in f:
                key = key.replace('\n', '')
                keys.append(key)
            if keys[-1] == '':
                del keys[-1]
            assert len(keys) == dict_size

    data = []
    for i, x in enumerate(keys):
        if data_as_objects:
            data.append((x, Foo(i)))
        else:
            data.append((x, i))
    return data


def get_dict(amount, data_as_objects, start=0):
    if start == 0:
        d = dict(get_dict_data(amount, data_as_objects))
    else:
        d = dict(_get_overlapping_dict(amount, start, data_as_objects))
    return d


def get_list(amount, data_as_objects):
    if data_as_objects:
        data = [Foo(x) for x in range(amount)]
    else:
        data = [x for x in range(amount)]
    return data


UNSORTED_LIST_FILE_NAME = "unsorted_lists/unsorted_list_{}.txt"


def get_unsorted_list(amount, data_as_objects):
    file_name = UNSORTED_LIST_FILE_NAME.format(amount)
    if not os.path.isfile(file_name):
        with open(file_name, mode='w') as f:
            unsorted_list = random.sample(range(amount), amount)
            for value in unsorted_list:
                f.write(str(value))
                f.write(' ')

    else:
        with open(file_name, mode='r+') as f:
            unsorted_list = f.readline()
            unsorted_list = unsorted_list.split(' ')
            if unsorted_list[-1] == '':
                del unsorted_list[-1]
            unsorted_list = [int(x) for x in unsorted_list]
            assert len(unsorted_list) == amount

    if data_as_objects:
        unsorted_list = [Foo(x) for x in unsorted_list]
    return unsorted_list


def get_unsorted_tuple(amount, data_as_objects):
    unsorted_list = get_unsorted_list(amount, data_as_objects)
    return tuple(unsorted_list)

