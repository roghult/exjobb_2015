#!/usr/bin/env bash

function run_function {
    # dict
    if [ $FUNCTION = "dict" ] || [ $FUNCTION = "all" ]; then
        function="dict"
        ACTIONS=("insert" "overwrite" "read")
        run

        ACTIONS=("merge")
        OLD_ARGUMENTS=$ARGUMENTS
        ARGUMENTS="$OLD_ARGUMENTS -m merge_all_match"
        run

        ARGUMENTS="$OLD_ARGUMENTS -m merge_half_match"
        run

        ARGUMENTS="$OLD_ARGUMENTS -m merge_none_match"
        run
        ARGUMENTS=$OLD_ARGUMENTS
    fi

    # list
    if [ $FUNCTION = "list" ] || [ $FUNCTION = "all" ]; then
        function="list"
        ACTIONS=("list_comprehension" "append" "sort")
        run
    fi

    # generator
    if [ $FUNCTION = "generator" ] || [ $FUNCTION = "all" ]; then
        function="generator"
        ACTIONS=("generate")
        run
    fi

    #objects
    if [ $FUNCTION = "objects" ] || [ $FUNCTION = "all" ]; then
        function="objects"
        ACTIONS=("generate" "add")
        run
    fi

    # tuple
    if [ $FUNCTION = "tuple" ] || [ $FUNCTION = "all" ]; then
        function="tuple"
        ACTIONS=("append" "sort")
        run
    fi

}

function run {
    for action in "${ACTIONS[@]}"
    do
        if [ $function = "tuple" ] && [ $action = "append" ] && [ $amount -gt 1000000 ]; then
            #do nothing
            :
        else
            for n in $(seq $ITERATIONS)
            do
                echo "$INTERPRETER main.py -p time $ARGUMENTS $function $action"
                if [ $function != "objects" ]; then
                    # run for integers
                    date
                    echo "Running integers"
                    $INTERPRETER main.py -p time $ARGUMENTS $function $action
                fi
                # run for objects
                date
                echo "Running objects"
                $INTERPRETER main.py -p time -o $ARGUMENTS $function $action
    #            $INTERPRETER -m memory_profiler main.py -p memory $ARGUMENTS $function $action
            done
        fi
    done
}

function run_interpreter {
    interpreter=$1
    if [ $interpreter = "cython" ]; then
            cythonize actions
            cd ..
            python setup.py build_ext -i
            cd src

            for amount in "${AMOUNTS[@]}"
            do
                INTERPRETER="python"
                ARGUMENTS="$BASE_ARGUMENTS -n $amount -i cython"
                run_function
                echo "Complete $interp for $amount"
            done

            # remove cythonized files
            rm -rf ../Build
            rm -rf actions/**/*.c
            rm -rf data/**/*.c
            rm -rf actions/**/*.cpp
            rm -rf data/**/*.cpp
            rm -rf actions/**/*.so
            rm -rf data/**/*.so
        else
            if [ $interpreter = "jython" ]; then
                interpreter="jython -J-Xms8g"
            fi

            for amount in "${AMOUNTS[@]}"
            do
                INTERPRETER=$interpreter
                ARGUMENTS="$BASE_ARGUMENTS -n $amount -i $INTERPRETER"
                run_function
                echo "Complete $interp for $amount"
            done
        fi
}

ARGUMENTS=""
ITERATIONS=5
INTERPRETER="all"
DEBUG=false
FUNCTION="all"
ACTION="all"
#AMOUNT=0

for i in "$@"
do
case $i in
    -i=*|--interpreter=*)
    INTERPRETER="${i#*=}"
    shift # past argument=value
    ;;
    -f=*|--function=*)
    FUNCTION="${i#*=}"
    shift # past argument=value
    ;;
    -a=*|--action=*)
    ACTION="${i#*=}"
    shift # past argument=value
    ;;
    -h|--help)
    echo "Help"
    shift # past argument with no value
    ;;
    -d|--debug)
    DEBUG=true
    ARGUMENTS="$ARGUMENTS --debug"
    shift # past argument with no value
    ;;
    -b)
    ARGUMENTS="$ARGUMENTS -b"
    shift # past argument with no value
    ;;
#    -n=*|--amount=*)
#    AMOUNT="${i#*=}"
#    shift # past argument with no value
#    ;;
    -iter=*)
    ITERATIONS="${i#*=}"
    shift
    ;;
    *)
            # unknown option
    ;;
esac
done

echo "Running benchmarks with following parameters:"
echo "Interpreter = $INTERPRETER"
echo "Function = $FUNCTION"
echo "Action = $ACTION"
echo "Iterations = $ITERATIONS"
#echo "Data amount = $AMOUNT"
echo "Debug = $DEBUG"
echo

AMOUNTS=(100 1000 10000 100000 1000000 5000000)
BASE_ARGUMENTS=$ARGUMENTS

date

if [ $INTERPRETER = "all" ]; then
    INTERPRETERS=("pypy" "python" "cython" "jython")
    for interp in "${INTERPRETERS[@]}"
    do
        run_interpreter $interp
    done
else
    run_interpreter $INTERPRETER
fi

date
