import time

import util
from data import Foo
import java.util.ArrayList as ArrayList


# @util.profiler()
def objects_generate(amount):
    action_start_time = time.clock()
    empty_list = []
    total_elapsed_time = 0
    for i in range(amount):
        start = time.clock()

        instance = Foo(i)

        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

        empty_list.append(instance)

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(empty_list)
    return total_elapsed_time


# @util.profiler()
def objects_add(object_list1, object_list2):
    action_start_time = time.clock()
    array_list1 = ArrayList(object_list1)
    array_list2 = ArrayList(object_list2)
    result_array_list = ArrayList()
    total_elapsed_time = 0
    for i, x in enumerate(object_list1):
        start = time.clock()

        result = array_list1.get(i) + array_list2.get(i)

        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

        result_array_list.add(result)

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(result_array_list)
    return total_elapsed_time
