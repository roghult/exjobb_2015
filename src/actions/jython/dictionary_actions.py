import time

import util
import java.util.HashMap as HashMap


# @util.profiler()
def dictionary_insert(dict_object, data_values):
    action_start_time = time.clock()
    hash_map = HashMap(dict_object)
    total_elapsed_time = 0
    for a, b in data_values:
        start = time.clock()
        hash_map.put(a, b)
        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(hash_map)
    return total_elapsed_time


# @util.profiler()
def dictionary_merge(dict1, dict2):
    hash_map1 = HashMap(dict1)
    hash_map2 = HashMap(dict2)

    start = time.clock()
    hash_map1.putAll(hash_map2)
    stop = time.clock()
    elapsed_time = stop - start

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(hash_map1)
    return elapsed_time


# @util.profiler()
def dictionary_read_all_values(dict_object):
    action_start_time = time.clock()
    hash_map = HashMap(dict_object)
    keys = dict_object.keys()
    total_elapsed_time = 0
    for key in keys:
        start = time.clock()
        value = hash_map.get(key)
        stop = time.clock()
        elapsed_time = stop - start
        # print to devnull to prevent result_list from potentially being removed in optimization
        util.print_to_devnull(value)
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

    return total_elapsed_time
