import time

import util


# @util.profiler()
def tuples_append(values):
    action_start_time = time.clock()
    total_elapsed_time = 0
    tup = ()
    for x in values:
        start = time.clock()

        tup = tup + (x,)

        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(tup)
    return total_elapsed_time


# @util.profiler()
def tuples_sort(tup, data_as_objects):
    if data_as_objects:
        start = time.clock()
        sorted_tuple = sorted(tup, key=lambda x: x.index, reverse=False)
        stop = time.clock()
    else:
        start = time.clock()
        sorted_tuple = sorted(tup)
        stop = time.clock()

    elapsed_time = stop - start
    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(sorted_tuple)
    return elapsed_time
