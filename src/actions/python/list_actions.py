import time
from data import Foo

import util


# @util.profiler()
def list_comprehension(amount, data_as_objects):

    if data_as_objects:
        start = time.clock()
        l = [Foo(x) for x in range(amount)]
        stop = time.clock()
    else:
        start = time.clock()
        l = [x for x in range(amount)]
        stop = time.clock()

    elapsed_time = stop - start
    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(l)
    return elapsed_time


# @util.profiler()
def list_append(values):
    action_start_time = time.clock()
    l = []
    total_elapsed_time = 0
    for x in values:
        start = time.clock()

        l.append(x)

        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(values)
    return total_elapsed_time


# @util.profiler()
def list_sort(l, data_as_objects):
    if data_as_objects:
        start = time.clock()
        sorted_list = sorted(l, key=lambda x: x.index, reverse=False)
        stop = time.clock()
    else:
        start = time.clock()
        sorted_list = sorted(l)
        stop = time.clock()

    elapsed_time = stop - start
    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(sorted_list)
    return elapsed_time
