import time

import util


# @util.profiler()
def generators_iterate(list values):
    action_start_time = time.clock()
    cdef int int_value
    generated_values = generators_generate(values)
    total_elapsed_time = 0
    run = True
    if util.get_data().DATA_AS_OBJECTS:
        while run:
            try:
                start = time.clock()
                value = generated_values.next()
                stop = time.clock()

                # print to devnull to prevent result_list from potentially being removed in optimization
                util.print_to_devnull(value)

                elapsed_time = stop - start
                total_elapsed_time += elapsed_time
                action_elapsed_time = stop - action_start_time
                if action_elapsed_time > util.get_data().TIME_LIMIT:
                    print
                    print "Test passed upper time limit, exiting"
                    print
                    return util.get_data().TIME_LIMIT
            except StopIteration:
                run = False
    else:
        while run:
            try:
                start = time.clock()
                int_value = generated_values.next()
                stop = time.clock()

                # print to devnull to prevent result_list from potentially being removed in optimization
                util.print_to_devnull(int_value)

                elapsed_time = stop - start
                total_elapsed_time += elapsed_time
                action_elapsed_time = stop - action_start_time
                if action_elapsed_time > util.get_data().TIME_LIMIT:
                    print
                    print "Test passed upper time limit, exiting"
                    print
                    return util.get_data().TIME_LIMIT
            except StopIteration:
                run = False

    return total_elapsed_time


def generators_generate(list values):
    cdef int int_value
    if util.get_data().DATA_AS_OBJECTS:
        for value in values:
            yield value
    else:
        for int_value in values:
            yield int_value
