import time

import util


# @util.profiler()
def dictionary_insert(dict dict_object, list data_values):
    action_start_time = time.clock()
    total_elapsed_time = 0
    cdef bytes a
    cdef int i
    if util.get_data().DATA_AS_OBJECTS:
        for a, ob in data_values:
            start = time.clock()
            dict_object[a] = ob
            stop = time.clock()
            elapsed_time = stop - start
            total_elapsed_time += elapsed_time
            action_elapsed_time = stop - action_start_time
            if action_elapsed_time > util.get_data().TIME_LIMIT:
                print
                print "Test passed upper time limit, exiting"
                print
                return util.get_data().TIME_LIMIT
    else:
        for a, i in data_values:
            start = time.clock()
            dict_object[a] = i
            stop = time.clock()
            elapsed_time = stop - start
            total_elapsed_time += elapsed_time
            action_elapsed_time = stop - action_start_time
            if action_elapsed_time > util.get_data().TIME_LIMIT:
                print
                print "Test passed upper time limit, exiting"
                print
                return util.get_data().TIME_LIMIT

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(dict_object)
    return total_elapsed_time


# @util.profiler()
def dictionary_merge(dict dict1, dict dict2):
    start = time.clock()
    dict1.update(dict2)
    stop = time.clock()
    elapsed_time = stop - start

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(dict1)
    return elapsed_time


# @util.profiler()
def dictionary_read_all_values(dict dict_object):
    action_start_time = time.clock()
    cdef list keys = dict_object.keys()
    cdef bytes key
    cdef int i
    total_elapsed_time = 0
    if util.get_data().DATA_AS_OBJECTS:
        for key in keys:
            start = time.clock()
            value = dict_object[key]
            stop = time.clock()
            elapsed_time = stop - start
            # print to devnull to prevent result_list from potentially being removed in optimization
            util.print_to_devnull(value)
            total_elapsed_time += elapsed_time
            action_elapsed_time = stop - action_start_time
            if action_elapsed_time > util.get_data().TIME_LIMIT:
                print
                print "Test passed upper time limit, exiting"
                print
                return util.get_data().TIME_LIMIT
    else:
        for key in keys:
            start = time.clock()
            i = dict_object[key]
            stop = time.clock()
            elapsed_time = stop - start
            # print to devnull to prevent result_list from potentially being removed in optimization
            util.print_to_devnull(i)
            total_elapsed_time += elapsed_time
            action_elapsed_time = stop - action_start_time
            if action_elapsed_time > util.get_data().TIME_LIMIT:
                print
                print "Test passed upper time limit, exiting"
                print
                return util.get_data().TIME_LIMIT

    return total_elapsed_time
