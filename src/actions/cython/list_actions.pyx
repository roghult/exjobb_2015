import time
from data.cython.data import Foo
import util
from libcpp.vector cimport vector


cdef vector[int] _get_int_vector(list l):
    cdef vector[int] vec
    cdef int x
    for x in l:
        vec.push_back(x)
    return vec


# @util.profiler()
def list_comprehension(int amount, data_as_objects):
    cdef int x
    cdef list l

    if data_as_objects:
        start = time.clock()
        l = [Foo(x) for x in range(amount)]
        stop = time.clock()
    else:
        start = time.clock()
        l = [x for x in range(amount)]
        stop = time.clock()

    elapsed_time = stop - start
    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(l)
    return elapsed_time


# @util.profiler()
def list_append(list values):
    action_start_time = time.clock()
    cdef vector[int] from_int_vector
    cdef vector[int] to_int_vector
    cdef int i
    cdef list l = []
    total_elapsed_time = 0

    if util.get_data().DATA_AS_OBJECTS:
        for ob in values:
            start = time.clock()

            l.append(ob)

            stop = time.clock()
            elapsed_time = stop - start
            total_elapsed_time += elapsed_time
            action_elapsed_time = stop - action_start_time
            if action_elapsed_time > util.get_data().TIME_LIMIT:
                print
                print "Test passed upper time limit, exiting"
                print
                return util.get_data().TIME_LIMIT
    else:
        from_int_vector = _get_int_vector(values)
        for i in from_int_vector:
            start = time.clock()

            to_int_vector.push_back(i)

            stop = time.clock()
            elapsed_time = stop - start
            total_elapsed_time += elapsed_time
            action_elapsed_time = stop - action_start_time
            if action_elapsed_time > util.get_data().TIME_LIMIT:
                print
                print "Test passed upper time limit, exiting"
                print
                return util.get_data().TIME_LIMIT


    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(to_int_vector)
    util.print_to_devnull(l)
    return total_elapsed_time


# @util.profiler()
def list_sort(list l, data_as_objects):
    cdef vector[int] vec
    if data_as_objects:
        start = time.clock()
        sorted_list = sorted(l, key=lambda x: x.index, reverse=False)
        stop = time.clock()
    else:
        vec = _get_int_vector(l)
        start = time.clock()
        sorted_list = sorted(vec)
        stop = time.clock()

    elapsed_time = stop - start
    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(sorted_list)
    return elapsed_time
