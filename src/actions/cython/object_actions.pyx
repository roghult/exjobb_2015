import time

import util
from data.cython.data import Foo


# @util.profiler()
def objects_generate(int amount):
    action_start_time = time.clock()
    cdef list empty_list = []
    cdef int i
    total_elapsed_time = 0
    for i in range(amount):
        start = time.clock()

        instance = Foo(i)

        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

        empty_list.append(instance)

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(empty_list)
    return total_elapsed_time


# @util.profiler()
def objects_add(list object_list1, list object_list2):
    action_start_time = time.clock()
    cdef list result_list = []
    cdef int i
    total_elapsed_time = 0
    for i, x in enumerate(object_list1):
        start = time.clock()

        result = object_list1[i] + object_list2[i]

        stop = time.clock()
        elapsed_time = stop - start
        total_elapsed_time += elapsed_time
        action_elapsed_time = stop - action_start_time
        if action_elapsed_time > util.get_data().TIME_LIMIT:
            print
            print "Test passed upper time limit, exiting"
            print
            return util.get_data().TIME_LIMIT

        result_list.append(result)

    # print to devnull to prevent result_list from potentially being removed in optimization
    util.print_to_devnull(result_list)
    return total_elapsed_time
