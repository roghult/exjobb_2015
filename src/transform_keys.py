from os import listdir
from os.path import isfile, join


SAME_LINE_KEYS_LOCATION = "dict_keys/"
NEW_LINE_KEYS_LOCATION = "dict_keys/"


def rewrite_key_files():
    same_line_key_files = [f for f in listdir(SAME_LINE_KEYS_LOCATION) if isfile(join(SAME_LINE_KEYS_LOCATION, f))]
    for key_file in same_line_key_files:
        file_path = SAME_LINE_KEYS_LOCATION + key_file
        with open(file_path, mode='r') as f:
            keys = f.readline()
            keys = keys.split(' ')
            if keys[-1] == '':
                del keys[-1]

        new_line_file = NEW_LINE_KEYS_LOCATION + key_file + "_new_line"
        with open(new_line_file, mode='w') as new_line_f:
            for key in keys:
                new_line_f.write(key)
                new_line_f.write('\n')


def main():
    rewrite_key_files()


if __name__ == '__main__':
    main()
