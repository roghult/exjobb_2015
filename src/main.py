import sys
import getopt

import util


def get_functions_and_help_texts():
    from functions import dictionary_function
    from functions import object_function
    from functions import generator_function
    from functions import list_function
    from functions import tuple_function
    functions = {'dict': dictionary_function.dictionary_function,
                 'list': list_function.list_function,
                 'generator': generator_function.generator_function,
                 'objects': object_function.object_function,
                 'tuple': tuple_function.tuple_function}

    help_texts = {'dict': dictionary_function.help_text,
                  'objects': object_function.help_text,
                  'generator': generator_function.help_text,
                  'list': list_function.help_text,
                  'tuple': tuple_function.help_text}

    return functions, help_texts


def print_help(functions):
    print "Available options:"
    print "-p <profile_method>  -->  set profile method (time or memory)"
    print "-h                   -->  help"
    print "-f <function>        -->  function help"
    print "-n <number>          -->  for amount of data values to be used"
    print "--debug              -->  for debug output"
    print
    print "Available functions:"
    print '\n'.join(functions.keys())


def main(argv):
    if len(argv) < 1:
        print "Insufficient parameters, run with -h for help."
        return

    if "-J-Xms8g" in argv:
        argv.remove("-J-Xms8g")

    try:
        opts, args = getopt.getopt(argv, "hobf:n:m:i:p:v", ["debug"])
    except getopt.GetoptError:
        print "Unknown option, exiting."
        sys.exit(2)

    amount = 10
    help_flag = False
    function_help_flag = False
    import data as python_data

    for opt, arg in opts:
        if opt == "-h":
            help_flag = True
        elif opt == "-f":
            function_help_flag = True
            function_for_help = arg
        elif opt == "-n":
            python_data.AMOUNT = int(arg)
            amount = int(arg)
        elif opt == "--debug":
            python_data.DEBUG = True
        elif opt == '-i':
            python_data.INTERPRETER = arg
        elif opt == '-p':
            python_data.PROFILE_METHOD = arg
        elif opt == '-b':
            python_data.BASE = True
        elif opt == '-o':
            python_data.DATA_AS_OBJECTS = True
        elif opt == '-m':
            python_data.MERGE_METHOD = arg

    functions, help_texts = get_functions_and_help_texts()

    if help_flag:
        print_help(functions)
        sys.exit()

    if function_help_flag:
        print help_texts[function_for_help]()
        sys.exit()

    util.copy_data()
    data = util.get_data()

    if data.PROFILE_METHOD is None or (data.PROFILE_METHOD != 'memory' and data.PROFILE_METHOD != 'time'):
        print "Must provide profile method 'memory' or 'time' with -p"
        sys.exit(2)

    functions, help_texts = get_functions_and_help_texts()

    if len(args) < 2:
        print "Insufficient number of parameters."
        print help_texts[args[0]]()
        sys.exit(2)

    function = args[0]
    data.FUNCTION = function
    data.ACTION = args[1]
    if function in functions:
        requested_function = functions[function]
        function_args = args[1:]
        requested_function(function_args, amount)
    else:
        print 'Unknown function \"{}\", exiting.'.format(function)


if __name__ == '__main__':
    main(sys.argv[1:])
