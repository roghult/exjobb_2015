import util

ACTION = "object_actions"


def _generate_objects(n, data_source):
    object_actions = util.get_actions(ACTION)
    elapsed_time = object_actions.objects_generate(n)
    util.log(util.get_time_logfile(), elapsed_time, True)


def _add_objects(n, data_source):
    object_list1 = data_source.get_list(n, True)
    object_list2 = data_source.get_list(n, True)
    object_actions = util.get_actions(ACTION)
    elapsed_time = object_actions.objects_add(object_list1, object_list2)
    util.log(util.get_time_logfile(), elapsed_time, True)

actions = {'generate': _generate_objects,
           'add': _add_objects}


def object_function(arguments, amount):
    first_arg = arguments[0]
    if first_arg in actions:
        # print 'Running {} on objects ({})'.format(first_arg, amount)
        objects_action = actions[first_arg]
        data = util.get_data()
        objects_action(amount, data)
    else:
        print 'Unknown action \"{}\" for objects function'.format(first_arg)
        return


def help_text():
    text = "Available actions for objects:\n"
    commands = '\n'.join(actions.keys())
    return text + commands
