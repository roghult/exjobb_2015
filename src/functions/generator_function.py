import util

ACTION = "generator_actions"


def _generate_values(n, data_source, data_as_objects):
    values = data_source.get_list(n, data_as_objects)
    generator_actions = util.get_actions(ACTION)
    elapsed_time = generator_actions.generators_iterate(values)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)

actions = {'generate': _generate_values}


def generator_function(arguments, amount):
    first_arg = arguments[0]
    if first_arg in actions:
        generator_action = actions[first_arg]
        data = util.get_data()
        data_as_objects = data.DATA_AS_OBJECTS
        generator_action(amount, data, data_as_objects)
    else:
        print 'Unknown action \"{}\" for generator function'.format(first_arg)
        return


def help_text():
    text = "Available actions for generator:\n"
    commands = '\n'.join(actions.keys())
    return text + commands
