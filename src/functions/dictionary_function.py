import util

ACTION = "dictionary_actions"


def _insert(n, data_source, data_as_objects):
    d = {}
    dict_data = data_source.get_dict_data(n, data_as_objects)
    dictionary_actions = util.get_actions(ACTION)
    elapsed_time = dictionary_actions.dictionary_insert(d, dict_data)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)
    util.debug(d)


def _overwrite(n, data_source, data_as_objects):
    dict_data = data_source.get_dict_data(n, data_as_objects)
    d = dict(dict_data)
    dictionary_actions = util.get_actions(ACTION)
    elapsed_time = dictionary_actions.dictionary_insert(d, dict_data)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)
    util.debug(d)


def _merge(n, data_source, data_as_objects):
    def merge(overlap_start, name):
        d1 = data_source.get_dict(n, data_as_objects)
        d2 = data_source.get_dict(n, data_as_objects, start=overlap_start)
        dictionary_actions = util.get_actions(ACTION)
        elapsed_time = dictionary_actions.dictionary_merge(d1, d2)
        util.log(util.get_time_logfile(), elapsed_time, data_as_objects, name=name)
        util.debug(d1)

    merge_methods = {
        'merge_all_match': 0,
        'merge_half_match': n/2,
        'merge_none_match': n
    }

    merge(merge_methods[data_source.MERGE_METHOD], data_source.MERGE_METHOD)


def _read(n, data_source, data_as_objects):
    d = data_source.get_dict(n, data_as_objects)
    dictionary_actions = util.get_actions(ACTION)
    elapsed_time = dictionary_actions.dictionary_read_all_values(d)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)


actions = {'insert': _insert,
           'overwrite': _overwrite,
           'merge': _merge,
           'read': _read}


def dictionary_function(arguments, amount):
    first_arg = arguments[0]
    if first_arg in actions:
        # print 'Running {} on dicts ({})'.format(first_arg, amount)
        dict_action = actions[first_arg]
        data = util.get_data()
        data_as_objects = data.DATA_AS_OBJECTS
        dict_action(amount, data, data_as_objects)
    else:
        print 'Unknown action \"{}\" for dict function'.format(first_arg)
        return


def help_text():
    text = "Available actions for dict:\n"
    commands = '\n'.join(actions.keys())
    return text + commands
