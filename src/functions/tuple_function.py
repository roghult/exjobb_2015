import util

ACTION = "tuple_actions"


def _append(n, data_source, data_as_objects):
    values = data_source.get_list(n, data_as_objects)
    tuple_actions = util.get_actions(ACTION)
    elapsed_time = tuple_actions.tuples_append(values)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)


def _sort(n, data_source, data_as_objects):
    tup = data_source.get_unsorted_tuple(n, data_as_objects)
    tuple_actions = util.get_actions(ACTION)
    elapsed_time = tuple_actions.tuples_sort(tup, data_as_objects)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)


actions = {'append': _append,
           'sort': _sort}


def tuple_function(arguments, amount):
    first_arg = arguments[0]
    if first_arg in actions:
        # print 'Running {} on tuples ({})'.format(first_arg, amount)
        dict_action = actions[first_arg]
        data = util.get_data()
        data_as_objects = data.DATA_AS_OBJECTS
        dict_action(amount, data, data_as_objects)
    else:
        print 'Unknown action \"{}\" for tuples function'.format(first_arg)
        return


def help_text():
    text = "Available actions for tuples:\n"
    commands = '\n'.join(actions.keys())
    return text + commands
