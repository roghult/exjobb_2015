import util

ACTION = "list_actions"


def _list_comprehension(n, data_source, data_as_objects):
    list_actions = util.get_actions(ACTION)
    elapsed_time = list_actions.list_comprehension(n, data_as_objects)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)


def _append(n, data_source, data_as_objects):
    values = data_source.get_list(n, data_as_objects)
    list_actions = util.get_actions(ACTION)
    elapsed_time = list_actions.list_append(values)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)


def _sort(n, data_source, data_as_objects):
    l = data_source.get_unsorted_list(n, data_as_objects)
    list_actions = util.get_actions(ACTION)
    elapsed_time = list_actions.list_sort(l, data_as_objects)
    util.log(util.get_time_logfile(), elapsed_time, data_as_objects)

actions = {'list_comprehension': _list_comprehension,
           'append': _append,
           'sort': _sort}


def list_function(arguments, amount):
    first_arg = arguments[0]
    if first_arg in actions and first_arg is not None:
        # print 'Running {} on lists ({})'.format(first_arg, amount)
        list_action = actions[first_arg]
        data = util.get_data()
        data_as_objects = data.DATA_AS_OBJECTS
        list_action(amount, data, data_as_objects)
    else:
        print 'Unknown action \"{}\" for list function'.format(first_arg)
        return


def help_text():
    text = "Available actions for lists:\n"
    commands = '\n'.join(actions.keys())
    return text + commands
